﻿using DEDVisualizerRedux.Classes.UI;
using DEDVisualizerRedux.Rewrite.Classes.EventListeners;
using DEDVisualizerRedux.Rewrite.Classes.Events;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Button;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace DEDVisualizerRedux {
    public class Visualizer : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D defaultTexture;
        Canvas UICanvas;
        SpriteFont spriteFont;

        public Visualizer () {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            //graphics.PreferMultiSampling = true;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize () {
            base.Initialize();

            UICanvas = new Canvas.Builder()
                .WithActiveState(true)
                .WithEnabledState(true)
                .WithGame(this)
                .WithLevel(0)
                .WithSize(new Rectangle(0, 0, 1920, 1080))
                .Build();

            //AbstractUIComponent thing = new AbstractUIComponent.Builder()
            //    .WithParent(UICanvas)
            //    .WithPadding(new Point(10, 10))
            //    .WithLevel(0)
            //    .WithBackground(defaultTexture)
            //    .WithSize(new Rectangle(500, 500, 250, 250))
            //    .Build();
            
            ToggleButton thing = new ToggleButton.ToggleBuilder()
                .WithText("My fancy toggle button")
                .WithSpriteFont(spriteFont)
                .WithActivatedColor(Color.White)
                .WithDefaultColor(Color.Gray)
                .WithMousedOverColor(Color.Aqua)
                .WithSize(new Rectangle(250, 250, 300, 100))
                .WithParent(UICanvas)
                .WithPadding(new Point(5, 5))
                .WithLevel(0)
                .WithBackground(defaultTexture)
                .Build();

            thing.AddListener(new SelectionListener() {
                OnSelection = (AbstractUIComponent component) => {
                    Debug.WriteLine("Selected");
                }
            });
        }

        protected override void LoadContent () {
            spriteFont = Content.Load<SpriteFont>("default");
            spriteBatch = new SpriteBatch(GraphicsDevice);
            defaultTexture = new Texture2D(GraphicsDevice, 1, 1);
            defaultTexture.SetData(new Color[] { Color.White });
        }

        protected override void UnloadContent () {
        }

        protected override void Update (GameTime gameTime) {
            base.Update(gameTime);
            
            EventRegistryFactory.GetEventRegistry().Update();
        }

        protected override void Draw (GameTime gameTime) {
            spriteBatch.Begin();
            GraphicsDevice.Clear(Color.Black);
            UICanvas.Draw(spriteBatch);

            base.Draw(gameTime);
            spriteBatch.End();
        }
    }
}
