﻿using DEDVisualizerRedux.Rewrite.Classes.Configuration;
using DEDVisualizerRedux.Rewrite.Classes.Persistence;
using DEDVisualizerRedux.Rewrite.Classes.Playlist;
using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using System;
using System.Diagnostics;

namespace DEDVisualizerRedux
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            EnvironmentSettings.Options.Parse(args);
            DatabaseManagerImpl mgr = DatabaseManagerImpl.GetInstance();
            ISong song = new Song.SongBuilder()
                .WithTitle("A test song")
                .WithDuration(360)
                .WithLocation("C:\\Temp")
                .Build();

            DatabaseDataManager.SaveSong(song);

            Debug.WriteLine(song);

            Debug.WriteLine("done");

            //using (var game = new Visualizer())
            //    game.Run();
        }
    }
#endif
}
