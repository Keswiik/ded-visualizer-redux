﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Interfaces.EventListeners
{
    /// <summary>
    /// A generic interface to be implemented by all event listeners.
    /// </summary>
    interface IEventListener
    {
    }
}
