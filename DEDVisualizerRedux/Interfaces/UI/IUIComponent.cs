﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DEDVisualizerRedux.Interfaces.EventListeners;
using Microsoft.Xna.Framework.Graphics;

namespace DEDVisualizerRedux.Interfaces.UI {
    delegate void EventExecutor<T> (T listener, IEventData eventData) where T : IEventListener;

    interface IUIComponent {
        /// <summary>
        ///     A <code>Rectangle</code> represending the size and location of the component
        /// </summary>
        Rectangle Size {
            get;
            set;
        }

        /// <summary>
        ///     The list of listeners registered to this component.
        /// </summary>
        List<IEventListener> EventListeners {
            get;
        }

        /// <summary>
        ///     Whether or not the component open to user interaction
        /// </summary>
        bool Enabled {
            get;
            set;
        }

        /// <summary>
        ///     Whether or not the component is active in the scene.
        ///     Inactive components will not be drawn, updated, or receive events.
        /// </summary>
        bool Active {
            get;
            set;
        }

        /// <summary>
        ///     The texture to use as this component's background.
        /// </summary>
        Texture2D Background {
            get;
            set;
        }

        /// <summary>
        ///     The parent of this component.
        ///     The top-level rendering component will have no parent.
        /// </summary>
        IUIComponent Parent {
            get;
        }

        /// <summary>
        ///     Whether or not the component intercepts all global events when focused.
        /// </summary>
        bool InterceptsGlobalEvents {
            get;
            set;
        }

        int Level {
            get;
            set;
        }

        /// <summary>
        ///     Used to update component state.
        /// </summary>
        void Update ();

        /// <summary>
        ///     Draws the component on the screen.
        /// </summary>
        void Draw (SpriteBatch spriteBatch);

        /// <summary>
        ///     Moves the component on the screen.
        /// </summary>
        /// <param name="delta">
        ///     The units (and direction) in which to move the component
        /// </param>
        void Move (Point delta);

        /// <summary>
        ///     Move the component to an absolute position on the screen
        /// </summary>
        /// <param name="position">
        ///     The new position for the component
        /// </param>
        void SetPosition (Point position);

        //void LayoutChildren();

        /// <summary>
        ///     Adds an event listener to the component
        /// </summary>
        /// <param name="listener">
        ///     The listener to add
        /// </param>
        void AddListener (IEventListener listener);

        /// <summary>
        ///     Removes a listener from the component
        /// </summary>
        /// <param name="listener">
        ///     The listener to remove
        /// </param>
        /// <returns>
        ///     <code>true</code> if the listener was removed, otherwise <code>false</code>
        /// </returns>
        bool RemoveListener (IEventListener listener);

        /// <summary>
        ///     A generic event that routes all events to their respective handlers
        /// </summary>
        /// <param name="eventData">
        ///     The data for the event
        /// </param>
        void OnEvent (IEventData eventData);

        /// <summary>
        ///     A typed method that fires events
        /// </summary>
        /// <typeparam name="T">
        ///     The type of <code>IEventListener</code> used by the event
        /// </typeparam>
        /// <param name="eventData">
        ///     The data for the event
        /// </param>
        /// <param name="eventExecutor">
        ///     A method which takes a typed <code>IEventListener</code> and <code>IEventData</code> 
        ///     and calls the appropriate listener methods.
        /// </param>
        void FireEvent<T> (IEventData eventData, EventExecutor<T> eventExecutor) where T : IEventListener;
    }
}
