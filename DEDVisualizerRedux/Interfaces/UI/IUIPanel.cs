﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Interfaces.UI
{
    interface IUIPanel
    {
        List<IUIComponent> Children { get; }

        void AddChild(IUIComponent uiComponent);

        bool RemoveChild(IUIComponent uiComponent);
    }
}
