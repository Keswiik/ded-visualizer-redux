﻿using DEDVisualizerRedux.Classes.UI;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Interfaces.UI {
    interface ICanvas : IUIComponent, IUIPanel {
        Game Game {
            get;
        }

        List<int> RegisteredLevels {
            get;
        }
    }
}
