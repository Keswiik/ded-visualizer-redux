﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Interfaces.UI {
    interface IInputStateManager {
        MouseState MouseState {
            get;
            set;
        }

        KeyboardState KeyboardState {
            get;
            set;
        }

        IEnumerable<IEventData> MouseEventQueue {
            get;
        }

        IEnumerable<IEventData> GlobalEventQueue {
            get;
        }

        IEnumerable<IEventData> FocusedEventQueue {
            get;
        }

        ICanvas Canvas {
            get;
            set;
        }

        IEnumerable<IEventData> GenerateMouseEvents (MouseState mouseState);

        IEnumerable<IEventData> GenerateKeyboardEvents (KeyboardState keyboardState);

        IUIComponent FindMousedOverComponent (IUIComponent focusedComponent, MouseState mouseState);

        IUIComponent FindMousedOverComponent (List<IUIComponent> allComponennts, MouseState mouseState);

        void GenerateEvents (IUIComponent focusedComponent);

        void GenerateEvents (IUIComponent focusedComponent, MouseState mouseState, KeyboardState keyboardState);
    }
}
