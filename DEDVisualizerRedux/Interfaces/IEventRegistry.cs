﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Interfaces
{
    interface IEventRegistry
    {
        IInputStateManager StateManager {
            get;
        }

        AbstractUIComponent FocusedComponent {
            get;
            set;
        }

        IUIComponent MousedOverComponent {
            get;
            set;
        }

        void DispatchEvents ();

        void Update ();
    }
}
