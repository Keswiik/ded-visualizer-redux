﻿using DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes;
using DEDVisualizerRedux.Rewrite.Interfaces.Persistence;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {

    /// <summary>
    ///     An abstract query builder with common functionality implemented for children to inherit.
    /// </summary>
    abstract class AbstractQueryBuilder : IQueryBuilder {

        /// <summary>
        ///     The target entity being updated by this builder
        /// </summary>
        internal object target;

        /// <summary>
        ///     The connection the built command will be using
        /// </summary>
        internal NpgsqlConnection connection;

        public AbstractQueryBuilder(object target, NpgsqlConnection connection) {
            this.target = target ?? throw new ArgumentNullException("Query builder target cannot be null");
            this.connection = connection ?? throw new ArgumentNullException("Query builder connection cannot be null");
        }

        /// <summary>
        ///     Retrieves the table name for a database entity
        /// </summary>
        /// <returns>The table name</returns>
        internal string GetTable() {
            object[] attributes = target.GetType().GetCustomAttributes(typeof(Table), true);

            if (attributes == null || attributes.Length == 0) {
                throw new InvalidOperationException("Queries cannot target classes without the Table attribute");
            }

            return (attributes[0] as Table).Name;
        }

        /// <summary>
        /// Retrieves the Column and PropertyInfo for the primary key of a database entity
        /// </summary>
        /// <param name="target">The entity to pull data from</param>
        /// <returns>The ColumnProperty instance, or null if it does not exist</returns>
        internal ColumnProperty GetPrimaryKey(object target) {
            // Information about all instance properties for the object being inspected
            PropertyInfo[] propertyInfo = target.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (PropertyInfo info in propertyInfo) {
                object[] attributes = info.GetCustomAttributes(typeof(Column), true);

                foreach (object attr in attributes) {
                    Column column = attr as Column;
                    if (column.PrimaryKey) {
                        return new ColumnProperty(info, column);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Convenience method to get primary key of builder target
        /// </summary>
        /// <returns>The primary key if found, otherwise null</returns>
        internal ColumnProperty GetPrimaryKey() {
            return GetPrimaryKey(target);
        }

        /// <summary>
        /// Gets all column data for a database entity
        /// Omits primary key
        /// </summary>
        /// <param name="target">The database entity to inspect</param>
        /// <returns>A List of ColumnProperty objects</returns>
        internal List<ColumnProperty> GetColumns(object target) {
            List<ColumnProperty> columns = new List<ColumnProperty>();
            PropertyInfo[] propertyInfo = target.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (PropertyInfo info in propertyInfo) {
                object[] attributes = info.GetCustomAttributes(typeof(Column), true);

                foreach (object attr in attributes) {
                    Column column = attr as Column;
                    if (!column.PrimaryKey) {
                        columns.Add(new ColumnProperty(info, column));
                    }
                }
            }

            return columns;
        }

        internal List<ColumnProperty> GetColumns() {
            return GetColumns(target);
        }

        /// <summary>
        /// Retrieves the value of a foreign key reference
        /// </summary>
        /// <param name="info">Property info about the foriegn key object instance</param>
        /// <param name="source">The object in which the foreign key resides</param>
        /// <returns>The primary key of the foreign object</returns>
        internal object GetForeignKeyValue(ColumnProperty info, object source) {
            object foreignDataSource = info.PropertyInfo.GetValue(source);
            
            // return null if foreign data source is null
            if (foreignDataSource == null) {
                return null;
            }

            ColumnProperty foreignPrimaryKey = GetPrimaryKey(foreignDataSource);

            // return null if foreign data source has no primary key
            if (foreignPrimaryKey == null) {
                return null;
            }

            return foreignPrimaryKey.PropertyInfo.GetValue(foreignDataSource);
        }

        public abstract NpgsqlCommand Build();
    }
}
