﻿using Npgsql;
using System;
using System.Collections.Generic;
using DEDVisualizerRedux.Rewrite.Classes.Playlist;
using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {

    /// <summary>
    ///     Factory class in charge of creating objects from database queries.
    /// </summary>
    static class DatabaseDataFactory {

        /// <summary>
        ///     Delegate to represent all builder functions for database entities
        /// </summary>
        /// <typeparam name="T">The type of object being built</typeparam>
        /// <param name="reader">The reader from which to pull data</param>
        /// <returns>An instance of T</returns>
        internal delegate T BuilderDelegate<T>(NpgsqlDataReader reader);

        private static int MINIMUM_ID = 1;

        /// <summary>
        ///     Fetches a single song from the database.
        /// </summary>
        /// <param name="id">The ID of the song to fetch</param>
        /// <returns>The song, if found</returns>
        public static ISong FetchSong(int id) {
            if (id < MINIMUM_ID) {
                return null;
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM songs WHERE id = " + id + " LIMIT 1;", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            ISong song = ReadOne(reader, BuildSongFromReader);
            reader.Close();

            return song;
        }

        public static ISong FetchSong(string path) {
            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM songs WHERE path = '" + path + "';", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            ISong song = ReadOne(reader, BuildSongFromReader);
            reader.Close();

            return song;
        }

        /// <summary>
        /// Fetches a single playlist form the database.
        /// </summary>
        /// <param name="id">The ID of the playlist to fetch</param>
        /// <returns>The playlist, if found</returns>
        public static IPlaylist FetchPlaylist(int id) {
            if (id < MINIMUM_ID) {
                return null;
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM playlist WHERE id = " + id + " LIMIT 1;", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            IPlaylist playlist = ReadOne(reader, BuildPlaylistFromReader);
            reader.Close();

            return playlist;
        }

        /// <summary>
        ///     Fetches a single genre from the database.
        /// </summary>
        /// <param name="id">The ID of the genre to fetch</param>
        /// <returns>The genre, if found</returns>
        public static IGenre FetchGenre(int id) {
            if (id < MINIMUM_ID) {
                return null;
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT genre_name FROM genres WHERE id = " + id + " LIMIT 1;", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            IGenre genre = ReadOne(reader, BuildGenreFromReader);
            reader.Close();

            return genre;
        }

        /// <summary>
        ///     Fetches a single album from the database
        /// </summary>
        /// <param name="id">The ID of the album to fetch</param>
        /// <returns>The album, if found</returns>
        public static IAlbum FetchAlbum(int id) {
            if (id < MINIMUM_ID) {
                return null;
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT * FROM albums WHERE id = " + id + " LIMIT 1;", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            IAlbum album = ReadOne(reader, BuildAlbumFromReader);
            reader.Close();

            return album;
        }

        /// <summary>
        ///     Fetches a single artist from the database
        /// </summary>
        /// <param name="id">The ID of the artist to fetch</param>
        /// <returns>The artist, if found</returns>
        public static IArtist FetchArtist(int id) {
            if (id < MINIMUM_ID) {
                return null;
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT artist_name FROM artists WHERE id = " + id + " LIMIT 1;", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            IArtist artist = ReadOne(reader, BuildArtistFromReader);
            reader.Close();

            return artist;
        }

        /// <summary>
        ///     Fetches all genres for a given song
        /// </summary>
        /// <param name="id">The ID of the song to find genres for</param>
        /// <returns>A List of genres (if any are found)</returns>
        public static List<IGenre> FetchGenresForSong(int id) {
            if (id < MINIMUM_ID) {
                return new List<IGenre>();
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand(@"SELECT genre_name FROM genres, genremap
                WHERE genres.id = genremap.genreID AND genremap.songID = " + id + ";", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            List<IGenre> genres = ReadMany(reader, BuildGenreFromReader);
            reader.Close();

            return genres;
        }

        /// <summary>
        ///     Fetches all artists for a song
        /// </summary>
        /// <param name="id">The ID of the song to find artists for</param>
        /// <returns>A List of artists (if any are found)</returns>
        public static List<IArtist> FetchArtistsForSong(int id) {
            if (id < MINIMUM_ID) {
                return new List<IArtist>();
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand(@"SELECT artist_name FROM artists, artistmap
                WHERE artists.id = artistmap.artistID AND artistmap.songID = " + id + ";", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            List<IArtist> artists = ReadMany(reader, BuildArtistFromReader);
            reader.Close();

            return artists;
        }

        /// <summary>
        ///     Fetches all songs for a playlist
        /// </summary>
        /// <param name="id">The ID of the playlist to find songs for</param>
        /// <returns>The playalist, if found</returns>
        public static List<ISong> FetchSongsForPlaylist(int id) {
            if (id < MINIMUM_ID) {
                return new List<ISong>();
            }

            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand(@"
                SELECT id, song_name, path, albumID, artistID, duration FROM songs, playlistmap
                WHERE songs.id = playlistmap.songID AND playlistmap.playlistID = " + id + ";", conn);
            
            NpgsqlDataReader reader = ExecuteQuery(cmd);
            List<ISong> songs = ReadMany(reader, BuildSongFromReader);
            reader.Close();

            return songs;
        }

        /// <summary>
        ///     Reads and builds multiple objects from a database query.
        /// </summary>
        /// <typeparam name="T">The type of object to build</typeparam>
        /// <param name="reader">The reader from which to pull data from</param>
        /// <param name="builder">The delegate that will handle building</param>
        /// <returns>A List of T objects</returns>
        internal static List<T> ReadMany<T>(NpgsqlDataReader reader, BuilderDelegate<T> builder) {
            List<T> items = new List<T>();
            if (reader.HasRows) {
                if (!reader.IsOnRow) {
                    reader.Read();
                }

                while (reader.IsOnRow) {
                    items.Add(builder(reader));
                    reader.Read();
                }
            }

            return items;
        }

        /// <summary>
        ///     Reads a single object from a database query.
        /// </summary>
        /// <typeparam name="T">The type of object to read</typeparam>
        /// <param name="reader">The reader from which to pull data</param>
        /// <param name="builder">The delegate that will handle building</param>
        /// <returns>An instance of T</returns>
        internal static T ReadOne<T>(NpgsqlDataReader reader, BuilderDelegate<T> builder) {
            if (!reader.HasRows) {
                throw new Exception("No id found for class " + typeof(T).Name);
            }

            if (!reader.IsOnRow) {
                reader.Read();
            }

            return builder(reader);
        }

        /// <summary>
        ///     Executes a query
        /// </summary>
        /// <param name="cmd">The command to run</param>
        /// <param name="conn">The connection to run the command through</param>
        /// <returns>A data reader</returns>
        private static NpgsqlDataReader ExecuteQuery(NpgsqlCommand cmd) {
            return cmd.ExecuteReader();
        }
        
        private static ISong BuildSongFromReader(NpgsqlDataReader reader) {
            return new Song.SongBuilder()
                .CopyFromReader(reader)
                .Build();
        }

        private static IAlbum BuildAlbumFromReader(NpgsqlDataReader reader) {
            return new Album.AlbumBuilder()
                .CopyFromReader(reader)
                .Build();
        }

        private static IPlaylist BuildPlaylistFromReader(NpgsqlDataReader reader) {
            return new Playlist.Playlist.PlaylistBuilder()
                .CopyFromReader(reader)
                .Build();
        }

        private static IArtist BuildArtistFromReader(NpgsqlDataReader reader) {
            return new Artist.ArtistBuilder()
                .CopyFromReader(reader)
                .Build();
        }

        private static IGenre BuildGenreFromReader(NpgsqlDataReader reader) {
            return new Genre.GenreBuilder()
                .CopyFromReader(reader)
                .Build();
        }
    }
}
