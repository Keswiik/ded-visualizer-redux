﻿using DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes;
using System;
using System.Reflection;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {
    class ColumnProperty {
        private PropertyInfo propertyInfo;
        private Column column;

        public ColumnProperty(PropertyInfo propertyInfo, Column column) {
            PropertyInfo = propertyInfo;
            Column = column;
        }

        public PropertyInfo PropertyInfo {
            get { return propertyInfo; }
            set {
                propertyInfo = value ?? throw new ArgumentNullException("PropertyInfo cannot be null");
            }
        }

        public Column Column {
            get { return column; }
            set {
                column = value ?? throw new ArgumentNullException("Column cannot be null");
            }
        }
    }
}
