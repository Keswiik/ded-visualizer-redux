﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {
    class InsertQueryBuilder : AbstractQueryBuilder {

        public InsertQueryBuilder(object target, NpgsqlConnection connection) : base(target, connection) { }

        public override NpgsqlCommand Build() {

            return GenerateCommand();
        }

        private NpgsqlCommand GenerateCommand() {
            NpgsqlCommand cmd = new NpgsqlCommand(GenerateQueryString(), connection);
            List<ColumnProperty> columns = GetColumns();

            // generating command parameters for each column using attribute metadata
            for (int i = 0; i < columns.Count; i++) {
                ColumnProperty column = columns[i];

                //no reason to save data that does not exist
                if (column.PropertyInfo.GetValue(target) == null) {
                    continue;
                }

                cmd.Parameters.Add(new NpgsqlParameter(column.Column.Name, column.Column.type));
                if (column.Column.ForeignKey) {
                    cmd.Parameters[cmd.Parameters.Count - 1].Value = GetForeignKeyValue(column, target);
                } else {
                    cmd.Parameters[cmd.Parameters.Count - 1].Value = column.PropertyInfo.GetValue(target);
                }
            }

            return cmd;
        }

        /// <summary>
        /// Generates an insert query string, skippinng null values
        /// </summary>
        /// <returns>The generated query string</returns>
        private string GenerateQueryString() {
            string table = GetTable();
            ColumnProperty primaryKey = GetPrimaryKey();
            List<ColumnProperty> columns = GetColumns();
            StringBuilder builder = new StringBuilder();

            // beginning INSERT INTO <table> statement
            builder.AppendLine(String.Format("INSERT INTO {0}", table));

            StringBuilder columnNames = new StringBuilder();
            StringBuilder columnValues = new StringBuilder();
            columnNames.Append("(");
            columnValues.Append("(");
            
            // build (<col_name>, . . .) and (<col_val>, . . .) strings
            for (int i = 0; i < columns.Count; i++) {
                ColumnProperty column = columns[i];
                // no reason to save null props, skip
                if (column.PropertyInfo.GetValue(target) == null) {
                    continue;
                }

                string terminator = i == columns.Count - 1 ? "" : ",";
                columnNames.Append(column.Column.Name);
                columnNames.Append(terminator);
                columnValues.Append(":" + column.Column.Name);
                columnValues.Append(terminator);
            }

            columnNames.Append(")");
            columnValues.Append(")");

            // appending generated sets to main builder
            builder.Append(columnNames.ToString() + " ");
            builder.Append("values " + columnValues.ToString());
            builder.Append("\nreturning " + primaryKey.Column.name + ";");

            return builder.ToString();
        }
    }
}
