﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {

    /// <summary>
    /// IQueryBuilder designed to handle SQL UPDATE queries
    /// </summary>
    class UpdateQueryBuilder : AbstractQueryBuilder {
        public UpdateQueryBuilder(object target, NpgsqlConnection connection) : base(target, connection) { }

        public override NpgsqlCommand Build() {
            return GenerateCommand();
        }

        private NpgsqlCommand GenerateCommand() {
            NpgsqlCommand cmd = new NpgsqlCommand(GenerateQueryString(), connection);
            List<ColumnProperty> columns = GetColumns();

            // generating command parameters for each column, using attribute metadata
            for (int i = 0; i < columns.Count; i++) {
                ColumnProperty column = columns[i];
                cmd.Parameters.Add(new NpgsqlParameter(column.Column.name, column.Column.Type));

                if (column.Column.ForeignKey) {
                    cmd.Parameters[cmd.Parameters.Count - 1].Value = GetForeignKeyValue(column, target);
                } else {
                    cmd.Parameters[cmd.Parameters.Count - 1].Value = column.PropertyInfo.GetValue(target);
                }
            }

            return cmd;
        }

        /// <summary>
        /// Utilizes table, primary key, and column data to generate a formatted SQL query string
        /// </summary>
        /// <returns>The query string</returns>
        private string GenerateQueryString() {
            string table = GetTable();
            ColumnProperty primaryKey = GetPrimaryKey();
            List<ColumnProperty> columns = GetColumns();
            StringBuilder builder = new StringBuilder();

            // specify update table
            builder.AppendLine(String.Format("UPDATE {0} SET", table));

            for (int i = 0; i < columns.Count; i++) {
                ColumnProperty column = columns[i];

                // generate set statement for each property "COLUMN_NAME = :COLUMN_NAME(,|)"
                builder.AppendLine(String.Format("{0} = :{1}{2}",
                    column.Column.Name,
                    column.Column.Name,
                    (i == columns.Count - 1) ? "" : ","));
            }

            // simple WHERE clause to specify update target by ID
            builder.AppendLine(String.Format("WHERE {0} = {0};", 
                primaryKey.Column.Name, 
                primaryKey.PropertyInfo.GetValue(target)));

            return builder.ToString();
        }
    }
}
