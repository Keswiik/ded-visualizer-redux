﻿using DEDVisualizerRedux.Rewrite.Classes.Configuration;
using Npgsql;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {
    internal class DatabaseConnectionManager {
        private string serverLocation;
        private string serverPort;
        private string databasePassword;
        private string databaseUser;

        public DatabaseConnectionManager() {
            serverLocation = EnvironmentSettings.ServerLocation ?? throw new InvalidOperationException("No server location specified");
            serverPort = EnvironmentSettings.ServerPort ?? throw new InvalidOperationException("No server port specified");
            databaseUser = EnvironmentSettings.DatabaseUsername ?? throw new InvalidOperationException("No database username specified");
            databasePassword = EnvironmentSettings.DatabasePassword ?? throw new InvalidOperationException("No database password specified");
        }

        public NpgsqlConnection GetConnection(string database) {
            return new NpgsqlConnection(BuildConnectionString(database));
        }

        public NpgsqlConnection GetConnection() {
            return GetConnection("postgres");
        }

        private string BuildConnectionString(string database) {
            return String.Format("Server={0};Port={1};Username={2};Password={3};Database={4};", 
                serverLocation, serverPort, databaseUser, databasePassword, database);
        }
    }
}
