﻿using DEDVisualizerRedux.Rewrite.Classes.Playlist;
using DEDVisualizerRedux.Rewrite.Interfaces.Persistence;
using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {
    static class DatabaseDataManager {
        public static ISong SaveSong(ISong song) {
            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            SaveAlbum(song.Album);
            foreach (IArtist artist in song.Artists) {
                SaveArtist(artist);
            }
            foreach (IGenre genre in song.Genres) {
                SaveGenre(genre);
            }

            if (SongExists(song)) {
                return DatabaseDataFactory.FetchSong(song.Path);
            }

            NpgsqlCommand cmd = GetQueryForObject(song.Id, song, conn);
            NpgsqlDataReader reader = cmd.ExecuteReader();

            if (!reader.IsOnRow) {
                reader.Read();
            }

            song.Id = (int)reader["id"];

            foreach (IGenre genre in song.Genres) {
                SaveGenre(genre);
            }

            return song;

            //List<IGenre> genres = DatabaseDataFactory.FetchGenresForSong(song.Id);
            //IEnumerable<IGenre> deletedGenres = genres.Except(song.Genres);
            //IEnumerable<IGenre> addedGenres = song.Genres.Except(genres);

            //TODO: figure out how to update these relations in a nice way.
        }

        public static void SaveAlbum(IAlbum album) {
            if (album.Id < 0) {
                InsertAlbum(album);
            } else {
                UpdateAlbum(album);
            }
        }

        public static void SavePlaylist(IPlaylist playlist) {
            if (playlist.Id < 0) {
                InsertPlaylist(playlist);
            } else {
                UpdatePlaylist(playlist);
            }
        }

        public static void SaveArtist(IArtist artist) {
            if (artist.Id < 0) {
                InsertArtist(artist);
            } else {
                UpdateArtist(artist);
            }
        }

        public static void SaveGenre(IGenre genre) {
            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();
        }

        private static void UpdateSong(ISong song) {
            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                UPDATE songs SET 
                    artistID = :ArtistID,
                    albumID = :AlbumID,
                    duration = :Duration,
                    song_name = :Title,
                    path = :Path,
                    year = :Year
                WHERE songs.id = " + song.Id + ";", conn);
        }

        private static void UpdateAlbum(IAlbum album) {

        }

        private static void UpdatePlaylist(IPlaylist playlist) {

        }

        private static void UpdateArtist(IArtist artist) {

        }

        private static void UpdateGenre(IGenre genre) {

        }

        private static void InsertSong(ISong song) {

        }

        private static void InsertAlbum(IAlbum album) {

        }

        public static void InsertPlaylist(IPlaylist playlist) {

        }

        private static void InsertArtist(IArtist artist) {

        }

        private static void InsertGenre(IGenre genre) {

        }

        private static void AddSongParameters(NpgsqlCommand cmd) {

        }

        private static void AddArtistParameters(NpgsqlCommand cmd) {

        }

        private static void AddGenreParameters(NpgsqlCommand cmd) {

        }

        private static void AddAlbumParameters(NpgsqlCommand cmd) {

        }

        private static void AddPlaylistParameters(NpgsqlCommand cmd) {

        }

        private static int ExecuteNonQuery(NpgsqlCommand cmd, NpgsqlConnection conn) {
            return cmd.ExecuteNonQuery();
        }

        private static bool SongExists(ISong song) {
            // Primarily here to prevent duplicate songs being added if you import multiple playlists with same song.
            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();
            NpgsqlCommand cmd = new NpgsqlCommand(String.Format(@"SELECT 1 FROM songs WHERE
                UPPER('{0}') = UPPER(path)", song.Path), conn);
            return cmd.ExecuteScalar() != null;
        }

        private static bool GenreExists(IGenre genre) {
            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand(String.Format(@"SELECT 1 FROM genres 
                WHERE UPPER('{0}') = UPPER(genre_name);", genre.Text), conn);
            return cmd.ExecuteScalar() != null;
        }

        private static NpgsqlCommand GetQueryForObject(int id, object obj, NpgsqlConnection conn) {
            IQueryBuilder builder = (id < 0) ? new InsertQueryBuilder(obj, conn) : (IQueryBuilder) new UpdateQueryBuilder(obj, conn);

            return builder.Build();
        }
    }
}
