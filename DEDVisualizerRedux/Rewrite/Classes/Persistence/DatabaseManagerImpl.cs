﻿using DEDVisualizerRedux.Rewrite.Interfaces.Persistence;
using Npgsql;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence {
    class DatabaseManagerImpl : IDatabaseManager {
        private static DatabaseManagerImpl INSTANCE;
        private DatabaseConnectionManager connectionManager;

        private DatabaseManagerImpl() {
            connectionManager = new DatabaseConnectionManager();
            Initialize();
        }

        public static DatabaseManagerImpl GetInstance() {
            if (INSTANCE == null) {
                INSTANCE = new DatabaseManagerImpl();
            }

            return INSTANCE;
        }

        public NpgsqlConnection GetConnection() {
            return connectionManager.GetConnection();
        }

        public NpgsqlConnection GetConnection(string database) {
            return connectionManager.GetConnection(database);
        }

        public void Initialize() {
            NpgsqlConnection conn = connectionManager.GetConnection();
            InitializeDatabase(conn);

            conn = connectionManager.GetConnection("visualizer");
            InitializeArtistTable(conn);
            InitializeAlbumTable(conn);
            InitializeSongTable(conn);
            InitializeArtistMap(conn);
            InitializePlaylistTable(conn);
            InitializePlaylistMapTable(conn);
            InitializeGenreTable(conn);
            InitializeGenreMapTable(conn);
        }

        private void InitializeDatabase(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"SELECT 1 FROM pg_database WHERE datname = 'visualizer';", conn);
            bool databaseExists = ExecuteScalarQuery(cmd, conn) != null;

            if (!databaseExists) {
                NpgsqlCommand createDbCmd = new NpgsqlCommand(@"
                    CREATE DATABASE visualizer
                    WITH OWNER = vis
                    ENCODING = 'UTF8'
                    CONNECTION LIMIT = -1;", conn);
                ExecuteNonQuery(createDbCmd, conn);
            }
        }

        private void InitializeSongTable(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS songs(
                    id          serial primary key,
                    albumID     int references albums(id),
                    duration    int,
                    song_name   varchar(50),
                    path        text,
                    year        int);", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void InitializeAlbumTable(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS albums(
                    id          serial primary key,
                    artistID    int references artists(id),
                    album_name  varchar(100),
                    year        int,
                    numTracks   int);", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void InitializeArtistTable(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS artists(
                    id          serial primary key,
                    artist_name varchar(100));", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void InitializePlaylistTable(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS playlists(
                    id              serial primary key,
                    path            text,
                    playlist_name   varchar(100));", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void InitializeGenreTable(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS genres(
                    id          serial primary key,
                    genre_name  varchar(50));", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void InitializePlaylistMapTable(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS playlistmap(
                    songID      int not null references songs(id),
                    playlistID  int not null references artists(id));", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void InitializeGenreMapTable(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS genremap(
                    songID      int not null references songs(id),
                    genreID     int not null references genres(id))", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void InitializeArtistMap(NpgsqlConnection conn) {
            NpgsqlCommand cmd = new NpgsqlCommand(@"
                CREATE TABLE IF NOT EXISTS artistmap(
                    songID      int not null references songs(id),
                    artistID    int not null references artists(id));", conn);

            ExecuteNonQuery(cmd, conn);
        }

        private void ExecuteNonQuery(NpgsqlCommand command, NpgsqlConnection connection) {
            if (connection.State != System.Data.ConnectionState.Open) {
                connection.Open();
            }
            command.ExecuteNonQuery();
        }

        private object ExecuteScalarQuery(NpgsqlCommand command, NpgsqlConnection connection) {
            if (connection.State != System.Data.ConnectionState.Open) {
                connection.Open();
            }
            return command.ExecuteScalar();
        }
    }
}
