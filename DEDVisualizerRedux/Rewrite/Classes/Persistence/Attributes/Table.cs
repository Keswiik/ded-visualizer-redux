﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes {
    class Table : Attribute {
        internal string name;

        public Table(string name) {
            Name = name;
        }

        public string Name {
            get { return name; }
            set {
                name = value ?? throw new ArgumentNullException("Invalid table name");
            }
        }
    }
}
