﻿using System;

namespace DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes {
    class Column : Attribute {
        internal string name;
        internal NpgsqlTypes.NpgsqlDbType type;
        internal bool primaryKey;
        internal bool foreignKey;

        public Column(string name, NpgsqlTypes.NpgsqlDbType type) {
            Name = name;
            Type = type;
        }

        public string Name {
            get { return name; }
            set {
                name = value ?? throw new ArgumentNullException("Invalid column name provided");
            }
        }

        public NpgsqlTypes.NpgsqlDbType Type {
            get { return type; }
            set { type = value; }
        }

        public bool PrimaryKey {
            get { return primaryKey; }
            set { primaryKey = value; }
        }

        public bool ForeignKey {
            get { return primaryKey; }
            set { foreignKey = value; }
        }
    }
}
