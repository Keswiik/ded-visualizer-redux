﻿using Mono.Options;

namespace DEDVisualizerRedux.Rewrite.Classes.Configuration {
    class EnvironmentSettings {
        private static string serverLocation = "localhost";
        private static string serverPort = "5432";
        private static string databaseUsername;
        private static string databasePassword;
        private static bool debug = false;

        public static OptionSet Options = new OptionSet {
            { "server=", server => { if (server != null) serverLocation = server; } },
            { "port=", port => { if (port != null) serverPort = port; } },
            { "user=", user => { if (user != null) databaseUsername = user; } },
            { "pw=", pw => {if (pw != null) databasePassword = pw; } },
            { "d", d => debug = d != null }
        };

        public static string ServerLocation {
            get { return serverLocation; }
        }

        public static string ServerPort {
            get { return serverPort; }
        }

        public static string DatabaseUsername {
            get { return databaseUsername; }
        }

        public static string DatabasePassword {
            get { return databasePassword; }
        }

        public static bool Debug {
            get { return debug; }
        }
    }
}
