﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using System;
using DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Canvas;
using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;

namespace DEDVisualizerRedux.Classes.UI {
    
    class Canvas : ICanvas {
        private bool enabled;
        private bool active;
        private int level;
        private Rectangle size;
        private Texture2D background;
        private Point padding;
        private Game game;
        private IUIPanel parent;
        private List<IUIComponent> children;
        private List<int> registeredLevels;

        public delegate void LevelHandler (int level);
        private static event LevelHandler RegisterLevelEvent;
        private static event LevelHandler DeregisterLevelEvent;

        private Canvas (Builder builder) {
            background = null;
            parent = null;
            Level = builder.level;
            Size = builder.size;
            game = builder.game;
            enabled = builder.enabled;
            active = builder.active;
            children = new List<IUIComponent>();
            registeredLevels = new List<int>();
            RegisterLevelEvent += AddLevel;

            CanvasRegistry.AddCanvas(level, this);
        }

        public Rectangle Size {
            get { return size; }
            set {
                if (value == null) {
                    throw new ArgumentNullException();
                }

                size = value;
            }
        }

        public bool Enabled {
            get { return enabled; }
            set { enabled = value; }
        }

        public bool Active {
            get { return active; }
            set { active = value; }
        }

        public Texture2D Background {
            get { return background; }
            set { }
        }

        public Point Padding {
            get { return padding; }
            set {
                padding = value != null ? value : new Point(0, 0);
            }
        }

        public IUIPanel Parent {
            get { return parent; }
        }

        public int Level {
            get { return level; }
            set {
                if (value != level) {
                    CanvasRegistry.MoveCanvas(value, this);
                    level = value;
                }
            }
        }

        public List<IUIComponent> Children {
            get { return children; }
        }

        public List<int> RegisteredLevels {
            get { return registeredLevels; }
        }

        public Game Game {
            get { return game; }
        }

        public void AddChild (IUIComponent uiComponent) {
            if (!children.Contains(uiComponent)) {
                children.Add(uiComponent);
            }
        }

        public bool RemoveChild (IUIComponent uiComponent) {
            return children.Remove(uiComponent);
        }

        public void Update () {
            //TODO: implement this at some point when I know what I'm doing.
        }

        public void Draw(SpriteBatch spriteBatch) {
            for (int i = registeredLevels.Count - 1; i >= 0; i--) {
                int level = registeredLevels[i];
                foreach(IUIComponent component in FindChildrenWithLevel(level)) {
                    component.Draw(spriteBatch);
                }
            }
        }

        /// <summary>
        /// Internal method to find all children with the specified level
        /// </summary>
        /// <param name="level">
        /// The level to find children on
        /// </param>
        /// <returns>
        /// The List<IUIComponent> children if found, or an empty list
        /// </returns>
        private List<IUIComponent> FindChildrenWithLevel(int level) {
            List<IUIComponent> childrenWithLevel = new List<IUIComponent>();
            foreach (IUIComponent child in children) {
                if (child.Level == level) {
                    childrenWithLevel.Add(child);
                }
            }

            return childrenWithLevel;
        }
        
        public void Move (Point delta) {
            size.Offset(delta);
        }

        public void AddLevel(int level) {
            if (!registeredLevels.Contains(level)) {
                registeredLevels.Add(level);
                registeredLevels.Sort(comparison: (int a, int b) => { return b - a; });
            }
        }

        public void SetPosition (Point position) {
            size.Location = position;
        }

        public void RemoveLevel(int level) {
            registeredLevels.Remove(level);
        }

        public static void ComponentAddToLevel(int level) {
            RegisterLevelEvent(level);
        }

        public static void ComponentRemovedFromLevel(int level) {
            DeregisterLevelEvent(level);
        }
        
        public List<IUIComponent> GetChildrenWithLevel(int level) {
            List<IUIComponent> matches = new List<IUIComponent>();

            foreach(IUIComponent component in children) {
                if (component.Level == level) {
                    matches.Add(component);
                }
            }

            return matches;
        }

        /// <summary>
        /// A builder for canvas objects
        /// </summary>
        public class Builder : ExpressionBuilder, IBuilder<Canvas> {
            internal Rectangle size;
            internal Texture2D background;
            internal int level;
            internal bool enabled;
            internal bool active;
            internal Game game;

            public Builder() {
                level = 0;
                enabled = true;
                active = true;
            }

            public Builder WithSize(Rectangle size) {
                VerifyBuildingState();
                this.size = size;
                return this;
            }

            public Builder WithLevel(int level) {
                VerifyBuildingState();
                this.level = level;
                return this;
            }

            public Builder WithEnabledState(bool enabled) {
                VerifyBuildingState();
                this.enabled = enabled;
                return this;
            }

            public Builder WithActiveState(bool active) {
                VerifyBuildingState();
                this.active = active;
                return this;
            }

            public Builder WithGame(Game game) {
                VerifyBuildingState();
                this.game = game;
                return this;
            }

            public Canvas Build () {
                VerifyBuildingState();
                SetBuildingComplete();
                return new Canvas(this);
            }
        }
    }
}
