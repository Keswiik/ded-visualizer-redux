﻿using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using System.Collections.Generic;

namespace DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Canvas {

    /// <summary>
    /// Tracks and manages all active canvases within a game
    /// </summary>
    class CanvasRegistry {

        private static Dictionary<int, List<ICanvas>> registry = new Dictionary<int, List<ICanvas>>();

        /// <summary>
        /// A registry mapping different canvases to their different UI levels
        /// </summary>
        public static Dictionary<int, List<ICanvas>> Registry {
            get { return registry; }
        }

        /// <summary>
        /// Adds a canvas to the registry
        /// </summary>
        /// <param name="level">
        ///     The level to add the canvas to
        /// </param>
        /// <param name="canvas">
        ///     The canvas to add
        /// </param>
        public static void AddCanvas(int level, ICanvas canvas) {
            if (!registry.ContainsKey(level)) {
                List<ICanvas> canvases = new List<ICanvas>();
                canvases.Add(canvas);
                registry.Add(level, canvases);
            } else if (!registry[level].Contains(canvas)) {
                registry[level].Add(canvas);
            }
        }

        /// <summary>
        /// Moves a canvas between UI layers.
        /// If the canvas is not currently contained within the registry, it will be added.
        /// </summary>
        /// <param name="level">
        ///     The level to move the canvas to
        /// </param>
        /// <param name="canvas">
        ///     The canvas instance to move
        /// </param>
        public static void MoveCanvas(int level, ICanvas canvas) {
            int currentLevel = FindCanvas(canvas);

            if (currentLevel != int.MinValue) {
                RemoveCanvas(currentLevel, canvas);
                AddCanvas(level, canvas);
            } else {
                AddCanvas(level, canvas);
            }
        }

        /// <summary>
        /// Removes a canvas from the registry
        /// </summary>
        /// <param name="level">
        ///     The level the canvas is located in
        /// </param>
        /// <param name="canvas">
        ///     The canvas to remove
        /// </param>
        public static void RemoveCanvas(int level, ICanvas canvas) {
            if (!registry.ContainsKey(level)) {
                return;
            }

            registry[level].Remove(canvas);
        }

        /// <summary>
        /// Gets all registered canvas levels
        /// </summary>
        /// <returns></returns>
        public static List<int> GetLevels() {
            List<int> keyList = new List<int>(registry.Keys);
            keyList.Sort((int a, int b) => { return b - a; });
            return keyList;
        }

        /// <summary>
        /// Finds the level a canvas is on
        /// </summary>
        /// <param name="canvas">
        ///     The canvas to find
        /// </param>
        /// <returns>
        ///     The level if found, otherwise int.MinValue
        /// </returns>
        private static int FindCanvas(ICanvas canvas) {
            foreach (int level in registry.Keys) {
                if (registry[level].Contains(canvas)) {
                    return level;
                }
            }

            return int.MinValue;
        }
    }
}
