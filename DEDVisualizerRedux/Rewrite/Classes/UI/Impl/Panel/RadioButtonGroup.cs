﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Button;
using System;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;

namespace DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Panel {

    /// <summary>
    /// Container and controller for RadioButtons
    /// </summary>
    class RadioButtonGroup : AbstractUIPanel {
        private int selectedIndex;
        private RadioButton selected;

        internal RadioButtonGroup(IUIBuilderComponent builder) : base(builder) { }

        /// <summary>
        /// Index of the currently selected button
        /// </summary>
        public int SelectedIndex {
            get { return selectedIndex; }
            set {
                if (selectedIndex < 0 || selectedIndex >= children.Count) {
                    throw new ArgumentOutOfRangeException("Invalid index");
                }

                selectedIndex = value;
            }
        }

        /// <summary>
        /// The currently selected RadioButton
        /// </summary>
        public RadioButton Selected {
            get {
                if (children == null || children.Count == 0) {
                    return null;
                } else if (selected == null) {
                    return (Children[0] as RadioButton);
                }

                return selected;
            }

            internal set {
                if (value == null) {
                    throw new ArgumentException("Selected cannot be null");
                }

                SelectedIndex = Children.IndexOf(value);
                selected = value;
            }
        }

        /// <summary>
        /// RadioButtonGroup only accepts RadioButton components as children
        /// </summary>
        /// <param name="uiComponent">
        ///     The component to add. ArgumentException thrown if param is not a RadioButton.
        /// </param>
        public override void AddChild (IUIComponent uiComponent) {
            if (!(uiComponent is RadioButton)) {
                throw new ArgumentException("Invalid child added");
            }

            base.AddChild(uiComponent);

            if ((uiComponent as RadioButton).Selected) {
                UpdateSelected(uiComponent as RadioButton);
            }
        }

        /// <summary>
        /// Updates Selected property and all RadioButtons in this group.
        /// </summary>
        /// <param name="selectedButton"></param>
        public void UpdateSelected(RadioButton selectedButton) {
            if (!children.Contains(selectedButton)) {
                throw new ArgumentException("RadioButton does not exist in group");
            }

            Selected = selectedButton;
            foreach (AbstractUIComponent child in children) {
                (child as RadioButton).Selected = child == selectedButton;
            }
        }

        public class RadioButtonGroupBuilder : UIBuilderComponent<RadioButtonGroupBuilder>, IBuilder<RadioButtonGroup> {
            public RadioButtonGroupBuilder() { }

            public RadioButtonGroup Build() {
                return new RadioButtonGroup(this);
            }
        }
    }
}
