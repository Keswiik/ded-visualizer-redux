﻿using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Classes.EventListeners;
using DEDVisualizerRedux.Rewrite.Classes.Input.Constants;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Button {
    class Button : AbstractUIComponent {
        private static Color DEFAULT_ACTIVATED_COLOR = Color.White;
        private static Color DEFAULT_MOUSED_OVER_COLOR = Color.Aqua;
        private static Color DEFAULT_COLOR = Color.LightGray;

        internal Color activatedColor;
        internal Color defaultColor;
        internal Color mousedOverColor;
        internal Color currentColor;
        internal SpriteFont spriteFont;
        internal bool mousedOver;
        internal bool activated;
        internal string text;

        internal Button(IButtonBuilderComponent builder) : 
            base(builder) {
            spriteFont = builder.SpriteFont ?? throw new ArgumentNullException("SpriteFont must be supplied");
            activatedColor = builder.ActivatedColor != null ? builder.ActivatedColor : DEFAULT_ACTIVATED_COLOR;
            defaultColor = builder.DefaultColor != null ? builder.DefaultColor : DEFAULT_COLOR;
            mousedOverColor = builder.MousedOverColor != null ? builder.MousedOverColor : DEFAULT_MOUSED_OVER_COLOR;
            currentColor = defaultColor;
            text = builder.Text ?? "";
            mousedOver = false;

            InitializeDefaultListeners();
        }

        internal virtual void InitializeDefaultListeners() {
            AddListener(new MouseListener() {
                MouseOver = (AbstractUIComponent component) => {
                    mousedOver = true;
                },

                MouseOff = (AbstractUIComponent component) => {
                    mousedOver = false;
                },

                MouseButtonDown = (AbstractUIComponent component, MouseButton button) => {
                    if (button == MouseButton.LeftButton) {
                        activated = true;
                    }
                },

                MouseButtonReleased = (AbstractUIComponent component, MouseButton button) => {
                    if (button == MouseButton.LeftButton) {
                        activated = false;
                    }
                }
            });
        }

        public override void Draw (SpriteBatch spriteBatch) {
            Color activeColor = GetActiveColor();

            spriteBatch.Draw(background, size, activeColor);
            spriteBatch.DrawString(spriteFont, text, GetTextPosition(), Color.Black);
        }

        internal Vector2 GetTextPosition() {
            Vector2 textSize = spriteFont.MeasureString(text);
            float x = (size.Width / 2) - (textSize.X / 2);
            float y = (size.Height / 2) - (textSize.Y / 2);

            return new Vector2(size.X + x, size.Y + y);
        }

        internal virtual Color GetActiveColor() {
            if (activated) {
                return activatedColor;
            } else if (mousedOver) {
                return mousedOverColor;
            }

            return defaultColor;
        }

        internal void CalculatePreferredSize() {
            Vector2 textSize = spriteFont.MeasureString(text);
            int x = padding.X * 2 + (int) textSize.X;
            int y = padding.Y * 2 + (int) textSize.Y;

            size = new Rectangle(size.X, size.Y, x, y);
        }

        internal interface IButtonBuilderComponent : IUIBuilderComponent {
            Color ActivatedColor { get; }
            Color DefaultColor { get; }
            Color MousedOverColor { get; }
            string Text { get; }
            SpriteFont SpriteFont { get; }
        }

        internal class ButtonComponentBuilder<T> : UIBuilderComponent<T>, IButtonBuilderComponent where T : ExpressionBuilder {
            public Color ActivatedColor { get; internal set; }
            public Color DefaultColor { get; internal set; }
            public Color MousedOverColor { get; internal set; }
            public string Text { get; internal set; }
            public SpriteFont SpriteFont { get; internal set; }

            public T WithActivatedColor(Color activatedColor) {
                VerifyBuildingState();
                ActivatedColor = activatedColor;

                return this as T;
            }

            public T WithDefaultColor(Color defaultColor) {
                VerifyBuildingState();
                DefaultColor = defaultColor;

                return this as T;
            }

            public T WithMousedOverColor(Color mousedOverColor) {
                VerifyBuildingState();
                MousedOverColor = mousedOverColor;

                return this as T;
            }

            public T WithText(string text) {
                VerifyBuildingState();
                Text = text;

                return this as T;
            }

            public T WithSpriteFont(SpriteFont spriteFont) {
                VerifyBuildingState();
                SpriteFont = spriteFont;

                return this as T;
            }
        }

        public class ButtonBuilder : ButtonComponentBuilder<ButtonBuilder>, IBuilder<Button> {

            public ButtonBuilder() {
                Enabled = true;
                Active = true;
                InterceptsGlobalEvents = false;
            }

            public Button Build () {
                VerifyBuildingState();
                SetBuildingComplete();

                return new Button(this);
            }
        }
    }
}
