﻿using DEDVisualizerRedux.Rewrite.Classes.EventListeners;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Panel;
using System;
using Microsoft.Xna.Framework;
using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;

namespace DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Button {

    /// <summary>
    /// Buttons that can be grouped and selectively enabled.
    /// </summary>
    class RadioButton : Button {
        private bool selected;

        internal RadioButton(IRadioButtonBuilderComponent builder) : base(builder) {
            if (!(parent is RadioButtonGroup)) {
                throw new ArgumentException("Invalid parent specified");
            }

            selected = builder.Selected;
        }

        public bool Selected {
            get { return selected; }
            set { selected = value; }
        }

        internal override void InitializeDefaultListeners () {
            base.InitializeDefaultListeners();

            AddListener(new SelectionListener() {
                OnSelection = (AbstractUIComponent component) => {
                    selected = true;
                    if ((parent as RadioButtonGroup).Selected != this) {
                        (parent as RadioButtonGroup).UpdateSelected(this);
      
                    }
                }
            });
        }

        internal override Color GetActiveColor () {
            if (activated || selected) {
                return activatedColor;
            } else if (mousedOver) {
                return mousedOverColor;
            }

            return defaultColor;
        }

        internal interface IRadioButtonBuilderComponent : IButtonBuilderComponent {
            bool Selected { get; }
        }

        internal class RadioButtonComponentBuilder<T> : ButtonComponentBuilder<T>, IRadioButtonBuilderComponent where T : ExpressionBuilder {
            public bool Selected { get; internal set; }

            public T AndSelected(bool selected) {
                VerifyBuildingState();
                Selected = selected;

                return this as T;
            }
        }

        public class RadioButtonBuilder : RadioButtonComponentBuilder<RadioButtonBuilder>, IBuilder<RadioButton> {
            public RadioButtonBuilder() {
                Selected = false;
                Active = true;
                Enabled = true;
                InterceptsGlobalEvents = false;
            }

            public RadioButton Build() {
                return new RadioButton(this);
            }
        }
    }
}
