﻿using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Classes.EventListeners;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using Microsoft.Xna.Framework;

namespace DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Button {

    /// <summary>
    /// Represents a button that has a toggleable on-off state
    /// </summary>
    class ToggleButton : Button {
        private bool isChecked;

        public ToggleButton(IToggleButtonBuilderComponent builder) : base(builder) {
            Checked = builder.Checked;
        }

        internal override void InitializeDefaultListeners () {
            base.InitializeDefaultListeners();

            AddListener(new SelectionListener() {
                OnSelection = (AbstractUIComponent component) => {
                    Checked = !Checked;
                }
            });
        }

        internal override Color GetActiveColor () {
            if (activated || Checked) {
                return activatedColor;
            } else if (mousedOver) {
                return mousedOverColor;
            }

            return defaultColor;
        }

        public bool Checked {
            get { return isChecked; }
            set { isChecked = value; }
        }

        internal interface IToggleButtonBuilderComponent : IButtonBuilderComponent {
            bool Checked { get; }
        }

        internal class ToggleButtonComponentBuilder<T> : ButtonComponentBuilder<T>, IToggleButtonBuilderComponent where T : ExpressionBuilder {
            public bool Checked { get; internal set; }

            public T IsChecked(bool isChecked) {
                VerifyBuildingState();
                Checked = isChecked;

                return this as T;
            }
        }

        public class ToggleBuilder : ToggleButtonComponentBuilder<ToggleBuilder>, IBuilder<ToggleButton> {
            public ToggleBuilder () {
                Checked = false;
                Active = true;
                Enabled = true;
                InterceptsGlobalEvents = false;
            }

            public ToggleButton Build() {
                return new ToggleButton(this);
            }
        }
    }
}
