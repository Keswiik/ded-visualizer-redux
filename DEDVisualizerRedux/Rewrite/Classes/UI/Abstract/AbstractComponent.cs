﻿using DEDVisualizerRedux.Classes.UI;
using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.UI.Abstract {
    class AbstractComponent : IComponent {
        internal Rectangle size;
        internal bool enabled;
        internal bool active;
        internal int level;
        internal Texture2D background;
        internal IUIPanel parent;
        internal Point padding;

        public AbstractComponent(IBuilderComponent builder) {
            Size = builder.Size;
            Padding = builder.Padding;
            Background = builder.Background;
            Enabled = builder.Enabled;
            Active = builder.Active;
            Level = builder.Level;
            parent = builder.Parent;
        }

        public int Level {
            get { return level; }
            set {
                Canvas.ComponentRemovedFromLevel(level);
                Canvas.ComponentAddToLevel(value);

                level = value;
            }
        }

        public Rectangle Size {
            get { return size; }
            set {
                if (value == null) {
                    throw new ArgumentNullException("Size cannot be null");
                } else if (value.Height <= 0 || value.Width <= 0) {
                    throw new ArgumentOutOfRangeException("Height and Width must be greater than 0");
                }

                size = value;
            }
        }

        public bool Enabled {
            get { return enabled; }
            set { enabled = value; }
        }

        public bool Active {
            get { return active; }
            set { active = value; }
        }

        public Texture2D Background {
            get { return background; }
            set {
                background = value ?? throw new ArgumentNullException("Background texture cannot be null");
            }
        }

        public IUIPanel Parent {
            get { return parent; }
        }

        public Point Padding {
            get { return padding; }
            set {
                padding = value != null ? value : new Point(0, 0);
            }
        }

        public void Move(Point delta) {
            size.Offset(delta);
        }

        public void SetPosition(Point position) {
            size.Location = position;
        }

        public virtual void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(background, size, Color.White);
        }
        
        public virtual void Update() { }

        internal interface IBuilderComponent {
            Rectangle Size { get; }
            bool Enabled { get; }
            bool Active { get; }
            int Level { get; }
            Texture2D Background { get; }
            IUIPanel Parent { get; }
            Point Padding { get; }
        }

        internal class BuilderComponent<T> : ExpressionBuilder, IBuilderComponent where T : ExpressionBuilder {
            public Rectangle Size { get; internal set; }
            public bool Enabled { get; internal set; }
            public bool Active { get; internal set; }
            public int Level { get; internal set; }
            public Texture2D Background { get; internal set; }
            public IUIPanel Parent { get; internal set; }
            public Point Padding { get; internal set; }


            public T WithSize(Rectangle size) {
                VerifyBuildingState();
                Size = size;

                return this as T;
            }

            public T AndEnabled(bool enabled) {
                VerifyBuildingState();
                Enabled = enabled;

                return this as T;
            }

            public T AndActive(bool active) {
                VerifyBuildingState();
                Active = active;

                return this as T;
            }

            public T WithLevel(int level) {
                VerifyBuildingState();
                Level = level;

                return this as T;
            }

            public T WithBackground(Texture2D background) {
                VerifyBuildingState();
                Background = background;

                return this as T;
            }

            public T WithParent(IUIPanel parent) {
                VerifyBuildingState();
                Parent = parent;

                return this as T;
            }

            public T WithPadding(Point padding) {
                VerifyBuildingState();
                Padding = padding;

                return this as T;
            }
        }

        public class ComponentBuilder : BuilderComponent<ComponentBuilder>, IBuilder<AbstractComponent> {
            public ComponentBuilder() {
                Enabled = true;
                Active = true;
            }

            public AbstractComponent Build() {
                VerifyBuildingState();
                SetBuildingComplete();

                return new AbstractComponent(this);
            }
        }
    }
}
