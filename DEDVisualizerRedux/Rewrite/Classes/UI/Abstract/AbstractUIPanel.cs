﻿using System.Collections.Generic;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using Microsoft.Xna.Framework.Graphics;

namespace DEDVisualizerRedux.Classes.UI.Abstract {
    class AbstractUIPanel : AbstractUIComponent, IUIPanel {
        internal List<IUIComponent> children;

        internal AbstractUIPanel (IUIBuilderComponent builder) : 
            base(builder) {
            children = new List<IUIComponent>();
        }

        public virtual List<IUIComponent> Children {
            get { return children; }
        }

        public virtual void AddChild (IUIComponent uiComponent) {
            if (!children.Contains(uiComponent)) {
                children.Add(uiComponent);
            }
        }

        public bool RemoveChild (IUIComponent uiComponent) {
            return children.Remove(uiComponent);
        }

        public override void Draw (SpriteBatch spriteBatch) {
            base.Draw(spriteBatch);

            foreach (IUIComponent child in children) {
                child.Draw(spriteBatch);
            }
        }

        class PanelBuilder : UIBuilderComponent<PanelBuilder>, IBuilder<AbstractUIPanel> {
            public PanelBuilder() {

            }

            public AbstractUIPanel Build() {
                VerifyBuildingState();
                SetBuildingComplete();

                return new AbstractUIPanel(this);
            }
        }
    }
}
