﻿using DEDVisualizerRedux.Classes.UI;
using DEDVisualizerRedux.Rewrite.Classes.EventListeners;
using DEDVisualizerRedux.Rewrite.Classes.Events;
using DEDVisualizerRedux.Rewrite.Classes.Events.Constants;
using DEDVisualizerRedux.Rewrite.Classes.Input.Constants;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using DEDVisualizerRedux.Rewrite.Interfaces.Events;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using sDEDVisualizerRedux.Rewrite.Classes.EventListeners;
using System;
using System.Collections.Generic;
using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using System.Diagnostics;

namespace DEDVisualizerRedux.Rewrite.Classes.UI.Abstract {
    class AbstractUIComponent : AbstractComponent, IUIComponent, IEventTarget {
        internal bool interceptsGlobalEvents;
        internal List<IEventListener> EVENT_LISTENERS;

        public AbstractUIComponent (IUIBuilderComponent builder) : base(builder) {
            interceptsGlobalEvents = builder.InterceptsGlobalEvents;
            EVENT_LISTENERS = new List<IEventListener>();
            NotifyParent();
        }

        internal virtual void NotifyParent () {
            parent.AddChild(this);
            if (parent is ICanvas) {
                ((Canvas) parent).AddLevel(level);
            }
        }

        public bool InterceptsGlobalEvents {
            get { return interceptsGlobalEvents; }
            set { interceptsGlobalEvents = value; }
        }

        public List<IEventListener> EventListeners {
            get { return EVENT_LISTENERS; }
        }

        public void AddListener (IEventListener listener) {
            if (!EVENT_LISTENERS.Contains(listener)) {
                EVENT_LISTENERS.Add(listener);
                if (listener is KeyListener) {
                    EventRegistryFactory.GetEventRegistry().GlobalEvent += OnEvent;
                }
            }
        }

        public bool RemoveListener (IEventListener listener) {
            if (listener is KeyListener && EVENT_LISTENERS.Contains(listener)) {
                EventRegistryFactory.GetEventRegistry().GlobalEvent -= OnEvent;
            }
            return EVENT_LISTENERS.Remove(listener);
        }

        public virtual void Draw (SpriteBatch spriteBatch) {
            spriteBatch.Draw(background, size, Color.White);
        }

        public void OnEvent (IEventData eventData) {
            switch (eventData.EventType) {
                case EventType.DRAG_START:
                    TakeFocus();
                    goto case EventType.DRAG;
                case EventType.DRAG:
                case EventType.DRAG_END:
                    FireEvent<DragListener>(eventData, FireDragEvent);
                    break;
                case EventType.DOUBLE_CLICK:
                    TakeFocus();
                    FireEvent<DoubleClickListener>(eventData, FireDoubleClickEvent);
                    break;
                case EventType.HOVER_START:
                case EventType.HOVER_END:
                    FireEvent<HoverListener>(eventData, FireHoverEvent);
                    break;
                case EventType.FOCUS_GAINED:
                case EventType.FOCUS_LOST:
                    FireEvent<FocusListener>(eventData, FireFocusEvent);
                    break;
                case EventType.MODIFIED:
                    FireEvent<ModifyListener>(eventData, FireModifyEvent);
                    break;
                case EventType.MOUSE_OVER:
                case EventType.MOUSE_OFF:
                case EventType.MOUSE_BUTTON_PRESSED:
                case EventType.MOUSE_BUTTON_HELD:
                case EventType.MOUSE_BUTTON_RELEASED:
                    TakeFocus();
                    FireEvent<MouseListener>(eventData, FireMouseEvent);
                    break;
                case EventType.SELECTED:
                    TakeFocus();
                    FireEvent<SelectionListener>(eventData, FireSelectionEvent);
                    break;
                case EventType.BUTTON_PRESSED:
                case EventType.BUTTON_RELEASED:
                    FireEvent<KeyListener>(eventData, FireKeyEvent);
                    break;
            }
        }

        private void TakeFocus() {
            EventRegistryFactory.GetEventRegistry().FocusedComponent = this;
        }

        public void FireEvent<T> (IEventData eventData, EventExecutor<T> eventExecutor) where T : IEventListener {
            foreach (IEventListener listener in EVENT_LISTENERS) {
                if (listener is T) {
                    eventExecutor((T) listener, eventData);
                }
            }
        }

        private void FireDragEvent (DragListener listener, IEventData eventData) {
            Vector2 mousePosition = eventData.GetData<Vector2>("mousePosition");
            switch (eventData.EventType) {
                case EventType.DRAG_START:
                    listener.OnDragStart(this, mousePosition);
                    break;
                case EventType.DRAG:
                    listener.OnDrag(this, mousePosition);
                    break;
                case EventType.DRAG_END:
                    listener.OnDragEnd(this, mousePosition);
                    break;
            }
        }

        private void FireDoubleClickEvent (DoubleClickListener listener, IEventData eventData) {
            listener.OnDoubleClick(this);
        }

        private void FireFocusEvent (FocusListener listener, IEventData eventData) {
            switch (eventData.EventType) {
                case EventType.FOCUS_GAINED:
                    listener.OnFocusGained(this);
                    break;
                case EventType.FOCUS_LOST:
                    listener.OnFocusLost(this);
                    break;
            }
        }

        private void FireHoverEvent (HoverListener listener, IEventData eventData) {
            switch (eventData.EventType) {
                case EventType.HOVER_START:
                    listener.OnHoverStart(this);
                    break;
                case EventType.HOVER_END:
                    listener.OnHoverEnd(this);
                    break;
            }
        }

        private void FireModifyEvent (ModifyListener listener, IEventData eventData) {
            listener.OnModify(this);
        }

        private void FireSelectionEvent (SelectionListener listener, IEventData eventData) {
            listener.OnSelection(this);
        }

        private void FireMouseEvent(MouseListener listener, IEventData eventData) {
            switch (eventData.EventType) {
                case EventType.MOUSE_OVER:
                    listener.MouseOver(this);
                    break;
                case EventType.MOUSE_OFF:
                    listener.MouseOff(this);
                    break;
                case EventType.MOUSE_BUTTON_PRESSED:
                    listener.MouseButtonDown(this, eventData.GetData<MouseButton>("button"));
                    break;
                case EventType.MOUSE_BUTTON_HELD:
                    listener.MouseButtonHeld(this, eventData.GetData<MouseButton>("button"));
                    break;
                case EventType.MOUSE_BUTTON_RELEASED:
                    listener.MouseButtonReleased(this, eventData.GetData<MouseButton>("button"));
                    break;
            }
        }

        private void FireKeyEvent(KeyListener listener, IEventData eventData) {
            Keys key = eventData.GetData<Keys>("button");
            if (eventData.EventType == EventType.BUTTON_PRESSED) {
                listener.OnKeyPressed(this, key);
            } else if (eventData.EventType == EventType.BUTTON_RELEASED) {
                listener.OnKeyReleased(this, key);
            }
        }

        internal interface IUIBuilderComponent : IBuilderComponent  {
            bool InterceptsGlobalEvents { get; }
        }

        internal class UIBuilderComponent<T> : BuilderComponent<T>, IUIBuilderComponent where T : ExpressionBuilder {
            public bool InterceptsGlobalEvents { get; internal set; }

            public T AndInterceptsGlobalEvents(bool intercepts) {
                VerifyBuildingState();
                InterceptsGlobalEvents = intercepts;

                return this as T;
            }
        }

        public class UIComponentBuilder : UIBuilderComponent<UIComponentBuilder>, IBuilder<AbstractUIComponent> {
            public UIComponentBuilder() {
                Enabled = true;
                Active = true;
                InterceptsGlobalEvents = true;
            }

            public AbstractUIComponent Build() {
                VerifyBuildingState();
                SetBuildingComplete();

                return new AbstractUIComponent(this);
            }
        }
    }
}
