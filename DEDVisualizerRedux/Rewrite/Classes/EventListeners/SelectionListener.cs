﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener to handle mouse selection (left-click) events
    /// </summary>
    class SelectionListener : IEventListener {

        /// <summary>
        /// A template for selection events to adhere to
        /// </summary>
        /// <param name="uiComponent">
        ///     The component that was selected
        /// </param>
        public delegate void SelectionDelegate (AbstractUIComponent uiComponent);

        public SelectionDelegate OnSelection;

        public SelectionListener() {
            OnSelection = OnSelectionImpl;
        }

        private void OnSelectionImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Component selected");
        }
    }
}
