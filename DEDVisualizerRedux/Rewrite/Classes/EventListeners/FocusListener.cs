﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener to handle focus gain/loss events.
    /// </summary>
    class FocusListener : IEventListener {

        /// <summary>
        /// A template for all focus events to adhere to
        /// </summary>
        /// <param name="uiComponent">
        ///     The element who's focus state has changed
        /// </param>
        public delegate void FocusDelegate (AbstractUIComponent uiComponent);

        public FocusDelegate OnFocusLost;
        public FocusDelegate OnFocusGained;

        public FocusListener () {
            OnFocusLost = OnFocusLostImpl;
            OnFocusGained = OnFocusGainedImpl;
        }

        private void OnFocusGainedImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Focus gained");
        }

        private void OnFocusLostImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Focus lost");
        }
    }
}
