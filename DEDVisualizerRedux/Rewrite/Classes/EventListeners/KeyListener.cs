﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace sDEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener for handling key presses
    /// </summary>
    class KeyListener : IEventListener {

        /// <summary>
        /// A template for all key events to adhere to
        /// </summary>
        /// <param name="uiComponent">
        ///     The component which received a key event
        /// </param>
        /// <param name="key">
        ///     The key the event was emitted for
        /// </param>
        public delegate void KeyDelegate (AbstractUIComponent uiComponent, Keys key);

        public KeyDelegate OnKeyPressed;
        public KeyDelegate OnKeyHeld;
        public KeyDelegate OnKeyReleased;

        public KeyListener () {
            OnKeyPressed = OnKeyPressedImpl;
            OnKeyHeld = OnKeyHeldImpl;
            OnKeyReleased = OnKeyReleasedImpl;
        }

        private void OnKeyHeldImpl(AbstractUIComponent uIComponent, Keys key) {
            Debug.WriteLine("Key " + key + " held");
        }

        private void OnKeyPressedImpl(AbstractUIComponent uiComponent, Keys key) {
            Debug.WriteLine("Key " + key + " pressed");
        }

        private void OnKeyReleasedImpl(AbstractUIComponent uiComponent, Keys key) {
            Debug.WriteLine("Key " + key + " released");
        }
    }
}
