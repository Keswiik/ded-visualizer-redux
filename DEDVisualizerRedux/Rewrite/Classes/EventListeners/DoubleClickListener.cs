﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener used to process double-clicks
    /// </summary>
    class DoubleClickListener : IEventListener {

        /// <summary>
        /// Template for all double click events to use
        /// </summary>
        /// <param name="uiComponent">The component that was clicked</param>
        public delegate void DoubleClickDelegate (AbstractUIComponent uiComponent);

        public DoubleClickDelegate OnDoubleClick;

        public DoubleClickListener() {
            OnDoubleClick = OnDoubleClickImpl;
        }
        
        private void OnDoubleClickImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Double clicked");
        }
    }
}
