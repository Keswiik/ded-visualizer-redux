﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener to handle events related to mouse cursor hovering
    /// </summary>
    class HoverListener : IEventListener {

        /// <summary>
        /// A template for all hover events to adhere to
        /// </summary>
        /// <param name="uiComponent">
        ///     The component whose hover state changed
        /// </param>
        public delegate void HoverDelegate (AbstractUIComponent uiComponent);

        public HoverDelegate OnHoverStart;
        public HoverDelegate OnHoverEnd;

        public HoverListener() {
            OnHoverStart = OnHoverStartImpl;
            OnHoverEnd = OnHoverEndImpl;
        }

        private void OnHoverStartImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Hover started");
        }

        private void OnHoverEndImpl (AbstractUIComponent uiComponent) {
            Console.WriteLine("Hover ended");
        }
    }
}
