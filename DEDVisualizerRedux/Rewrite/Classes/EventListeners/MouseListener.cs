﻿using DEDVisualizerRedux.Rewrite.Classes.Input.Constants;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using Microsoft.Xna.Framework.Input;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener for general mouse events
    /// </summary>
    class MouseListener : IEventListener {

        /// <summary>
        /// A template for general mouse events to adhere to
        /// </summary>
        /// <param name="component">
        ///     The component the mouse is over
        /// </param>
        public delegate void MouseDelegate (AbstractUIComponent component);

        /// <summary>
        /// A template for mouse button press events
        /// </summary>
        /// <param name="component">
        ///     The component that the mouse is over
        /// </param>
        /// <param name="button">
        ///     The button that emitted the event
        /// </param>
        public delegate void MouseButtonDelegate (AbstractUIComponent component, MouseButton button);

        public MouseDelegate MouseOver;
        public MouseDelegate MouseOff;
        public MouseButtonDelegate MouseButtonDown;
        public MouseButtonDelegate MouseButtonHeld;
        public MouseButtonDelegate MouseButtonReleased;

        public MouseListener() {
            MouseOver = MouseOverImpl;
            MouseOff = MouseOffImpl;
            MouseButtonDown = MouseButtonDownImpl;
            MouseButtonHeld = MouseButtonHeldImpl;
            MouseButtonReleased = MouseButtonReleasedImpl;
        }

        private void MouseOverImpl(AbstractUIComponent component) {
            Console.WriteLine("Mouse over");
        }

        private void MouseOffImpl(AbstractUIComponent component) {
            Console.WriteLine("Mouse over");
        }

        private void MouseButtonDownImpl(AbstractUIComponent component, MouseButton button) {
            Console.WriteLine("Mouse button down");
        }

        private void MouseButtonHeldImpl(AbstractUIComponent component, MouseButton button) {
            Console.WriteLine("Mouse button held");
        }

        private void MouseButtonReleasedImpl(AbstractUIComponent component, MouseButton button) {
            Console.WriteLine("Mouse button released");
        }
    }
}
