﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener used to track the modification of values on components (mainly text fields)
    /// </summary>
    class ModifyListener : IEventListener {

        /// <summary>
        /// A template for modify events to adhere to
        /// </summary>
        /// <param name="uiComponent">
        ///     The component that was modified
        /// </param>
        public delegate void ModifyDelegate (AbstractUIComponent uiComponent);

        public ModifyDelegate OnModify;

        public ModifyListener() {
            OnModify = OnModifyImpl;
        }

        private void OnModifyImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Modified");
        }
    }
}
