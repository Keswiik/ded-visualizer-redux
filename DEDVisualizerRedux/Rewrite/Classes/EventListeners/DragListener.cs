﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using Microsoft.Xna.Framework;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.EventListeners {

    /// <summary>
    /// An event listener used to handle all events related to dragging UI components
    /// </summary>
    class DragListener : IEventListener {

        /// <summary>
        /// A template for all drag events to adhere to
        /// </summary>
        /// <param name="uiComponent">
        ///     The component being dragged
        /// </param>
        /// <param name="mouseLocation">
        ///     The current location of the mouse
        /// </param>
        public delegate void DragDelegate (AbstractUIComponent uiComponent, Vector2 mouseLocation);

        public DragDelegate OnDragStart;
        public DragDelegate OnDrag;
        public DragDelegate OnDragEnd;

        public DragListener() {
            OnDragStart = OnDragStartImpl;
            OnDrag = OnDragImpl;
            OnDragEnd = OnDragEndImpl;
        }

        private void OnDragStartImpl(AbstractUIComponent uiComponent, Vector2 mouseLocation) {
            Console.WriteLine("Drag started");
        }

        private void OnDragImpl(AbstractUIComponent uIComponent, Vector2 mouseLocation) {
            Console.WriteLine("Dragging");
        }

        private void OnDragEndImpl(AbstractUIComponent uIComponent, Vector2 mouseLocation) {
            Console.WriteLine("Drag ended");
        }
    }
}
