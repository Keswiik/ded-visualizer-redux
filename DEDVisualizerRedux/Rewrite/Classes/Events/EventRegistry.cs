﻿using DEDVisualizerRedux.Rewrite.Classes.Events.Constants;
using DEDVisualizerRedux.Rewrite.Classes.Input;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.Events;
using DEDVisualizerRedux.Rewrite.Interfaces.Input;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using Microsoft.Xna.Framework.Input;

namespace DEDVisualizerRedux.Rewrite.Classes.Events {
    class EventRegistry : IEventRegistry {
        private AbstractUIComponent focusedComponent;
        private IUIComponent mousedOverComponent;
        private IInputStateManager stateManager;

        public delegate void GlobalEventHandler (IEventData eventData);
        public event GlobalEventHandler GlobalEvent;

        internal EventRegistry() {
            stateManager = InputStateManagerFactory.GetInputStateManager();
        }

        public AbstractUIComponent FocusedComponent {
            get { return focusedComponent; }
            set {
                if (value != focusedComponent) {
                    if (focusedComponent != null) {
                        focusedComponent.OnEvent(new EventData(EventType.FOCUS_LOST));
                    }
                    value.OnEvent(new EventData(EventType.FOCUS_GAINED));
                }

                focusedComponent = value;
            }
        }

        public IUIComponent MousedOverComponent {
            get { return mousedOverComponent; }
            set {
                if (value != null && value != mousedOverComponent) {
                    value.OnEvent(new EventData(EventType.MOUSE_OVER));
                }
                if (mousedOverComponent != null && value != mousedOverComponent) {
                    mousedOverComponent.OnEvent(new EventData(EventType.MOUSE_OFF));
                }

                mousedOverComponent = value;
            }
        }

        public void DispatchEvents () {
            if (mousedOverComponent != null) {
                foreach (IEventData eventData in stateManager.MouseEventQueue) {
                    MousedOverComponent.OnEvent(eventData);
                }
            }

            GlobalEventHandler handler;
            if (focusedComponent != null && focusedComponent.InterceptsGlobalEvents) {
                handler = focusedComponent.OnEvent;
            } else {
                handler = GlobalEvent;
            }

            foreach (IEventData eventData in stateManager.GlobalEventQueue) {
                handler(eventData);
            }

            if (focusedComponent != null) {
                foreach (IEventData eventData in stateManager.FocusedEventQueue) {
                    focusedComponent.OnEvent(eventData);
                }
            }
        }

        public void Update () {
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyboardState = Keyboard.GetState();
            stateManager.GenerateEvents(focusedComponent, mouseState, keyboardState);
            MousedOverComponent = stateManager.FindMousedOverComponent(focusedComponent, mouseState);
            DispatchEvents();
            stateManager.MouseState = mouseState;
            stateManager.KeyboardState = keyboardState;
        }
    }
}
