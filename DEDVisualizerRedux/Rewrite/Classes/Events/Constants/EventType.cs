﻿namespace DEDVisualizerRedux.Rewrite.Classes.Events.Constants {

    /// <summary>
    /// Represent different event types
    /// </summary>
    enum EventType {
        /// <summary>
        /// Mouse double-click
        /// </summary>
        DOUBLE_CLICK,

        /// <summary>
        /// Emitted when dragging starts
        /// </summary>
        DRAG_START,

        /// <summary>
        /// Emitted throughout a drag
        /// </summary>
        DRAG,

        /// <summary>
        /// Emitted once a drag has finished
        /// </summary>
        DRAG_END,

        /// <summary>
        /// Emitted when a component gains focus (clicked on, etc)
        /// </summary>
        FOCUS_GAINED,

        /// <summary>
        /// Emitted when a component loses focus
        /// </summary>
        FOCUS_LOST,

        /// <summary>
        /// Emitted at the very start of mouse hovering
        /// </summary>
        HOVER_START,

        /// <summary>
        /// Emitted when the mouse was previously hovering but has moved
        /// </summary>
        HOVER_END,

        /// <summary>
        /// Emitted when something (likely on a ui component) is modified
        /// </summary>
        MODIFIED,

        /// <summary>
        /// Emitted when a component is left-clicked
        /// </summary>
        SELECTED,

        /// <summary>
        /// Emitted when any mouse button is pressed
        /// </summary>
        MOUSE_BUTTON_PRESSED,

        /// <summary>
        /// Emitted while any mouse button is held down
        /// </summary>
        MOUSE_BUTTON_HELD,

        /// <summary>
        /// Emitted once any mouse button is released
        /// </summary>
        MOUSE_BUTTON_RELEASED,

        /// <summary>
        /// Emitted when the mouse initially moves over a component
        /// </summary>
        MOUSE_OVER,

        /// <summary>
        /// Emitted when the mouse moves off a component
        /// </summary>
        MOUSE_OFF,

        /// <summary>
        /// Emitted when any keyboard button is pressed
        /// </summary>
        BUTTON_PRESSED,

        /// <summary>
        /// Emitted while any keyboard button is held down
        /// </summary>
        BUTTON_HELD,

        /// <summary>
        /// Emitted when any keyboard button is released
        /// </summary>
        BUTTON_RELEASED
    }
}
