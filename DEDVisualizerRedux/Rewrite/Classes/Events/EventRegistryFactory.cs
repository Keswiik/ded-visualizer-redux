﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Classes.Events {
    class EventRegistryFactory {
        private static EventRegistry INSTANCE;

        public static EventRegistry GetEventRegistry() {
            if (INSTANCE == null) {
                INSTANCE = new EventRegistry();
            }

            return INSTANCE;
        }
    }
}
