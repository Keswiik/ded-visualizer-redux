﻿using DEDVisualizerRedux.Rewrite.Interfaces.Animation;
using System;
using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using Microsoft.Xna.Framework;
using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;

namespace DEDVisualizerRedux.Rewrite.Classes.Animation.Abstract {

    /// <summary>
    /// Represents an animation that can be played on an object.
    /// </summary>
    class Animation : IAnimation {

        /// <summary>
        /// Delegate for all update functions to adhere to
        /// </summary>
        /// <param name="gameTime">
        ///     The current game time
        /// </param>
        public delegate void UpdateFunction (GameTime gameTime);

        /// <summary>
        /// The component the animation will affect
        /// </summary>
        private AbstractUIComponent target;

        /// <summary>
        /// The time, in milliseconds, the animation will play for
        /// </summary>
        private long duration;

        /// <summary>
        /// The function to be invoked when updating the animation state
        /// </summary>
        private UpdateFunction updateFunction;

        internal Animation(IAnimationBuilderComponent builder) {
            duration = builder.Duration;
            target = builder.Target;
            updateFunction = builder.OnUpdate;
        }

        public AbstractUIComponent Target {
            get { return target; }
            internal set {
                if (value == null) {
                    throw new ArgumentNullException("Target cannot be null");
                }

                target = value;
            }
        }

        public long Duration {
            get { return duration; }
            internal set {
                if (value <= 0) {
                    throw new ArgumentOutOfRangeException("Animation duration must be greater than 0ms");
                }

                duration = value;
            }
        }

        public UpdateFunction OnUpdate {
            get { return updateFunction; }
            set {
                updateFunction = value ?? throw new ArgumentNullException("OnUpdate cannot be null");
            }
        }

        public virtual bool IsFinished () {
            return duration <= 0;
        }

        public virtual void Play(GameTime gameTime) {
            if (OnUpdate == null) {
                throw new Exception("No update function supplied");
            }

            OnUpdate(gameTime);

            Duration -= gameTime.ElapsedGameTime.Milliseconds;
        }
        
        /// <summary>
        /// Defines what values the animation builder will set
        /// </summary>
        internal interface IAnimationBuilderComponent {
            long Duration { get; }
            UpdateFunction OnUpdate { get; }
            AbstractUIComponent Target { get; }
        }

        /// <summary>
        /// Class defining the builder logic for each value
        /// </summary>
        /// <typeparam name="T">
        ///     The type of builder being used
        /// </typeparam>
        internal class ComponentBuilder<T> : ExpressionBuilder, IAnimationBuilderComponent where T : ExpressionBuilder {
            public long Duration { get; internal set; }
            public UpdateFunction OnUpdate { get; internal set; }
            public AbstractUIComponent Target { get; internal set; }

            public T WithDuration (long durationInMillis) {
                VerifyBuildingState();
                Duration = durationInMillis;

                return this as T;
            }

            public T WithUpdate (UpdateFunction updateFunction) {
                VerifyBuildingState();
                OnUpdate = updateFunction;

                return this as T;
            }

            public T WithTarget (AbstractUIComponent target) {
                VerifyBuildingState();
                Target = target;

                return this as T;
            }
        }

        /// <summary>
        /// Public class that makes use of internal builder logic
        /// Contains method to build target class instance
        /// </summary>
        public class AnimationBuilder : ComponentBuilder<AnimationBuilder>, IBuilder<Animation> {
            public AnimationBuilder() { }

            public Animation Build() {
                return new Animation(this);
            }
        }
    }
}
