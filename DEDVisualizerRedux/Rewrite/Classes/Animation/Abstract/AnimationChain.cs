﻿using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Animation;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DEDVisualizerRedux.Rewrite.Classes.Animation.Abstract {

    /// <summary>
    /// Represents an animation composed of multiple animations.
    /// They are played sequentially, in the order added.
    /// </summary>
    class AnimationChain : IAnimation {

        /// <summary>
        /// The animations to be played by the chain
        /// </summary>
        private List<IAnimation> animations;

        public AnimationChain(IAnimationChainBuilderComponent builder) {
            Animations = builder.Animations;
        }
        
        public List<IAnimation> Animations {
            get { return animations; }
            internal set {
                if (value == null || value.Count == 0) {
                    throw new ArgumentOutOfRangeException("At least one animation must be supplied to AnimationChain");
                }

                animations = value;
            }
        }

        public void Play (GameTime gameTime) {
            if (animations.Count == 0) {
                return;
            }

            animations.First().Play(gameTime);

            if (animations.First().IsFinished()) {
                Animations.RemoveAt(0);
            }
        }

        public bool IsFinished () {
            return animations.Count == 0;
        }

        public void AddAnimation(IAnimation animation) {
            if (animation == null) {
                throw new ArgumentNullException("Animation cannot be null");
            }

            animations.Add(animation);
        }

        /// <summary>
        /// Defines what values the animation chain builder will set
        /// </summary>
        internal interface IAnimationChainBuilderComponent {
            List<IAnimation> Animations { get; }
        }

        /// <summary>
        /// Defines the builder logic for setting values
        /// </summary>
        /// <typeparam name="T">
        ///     The type of builder being used
        /// </typeparam>
        internal class AnimationChainBuilderComponent<T> : ExpressionBuilder, IAnimationChainBuilderComponent where T : ExpressionBuilder {
            public List<IAnimation> Animations { get; internal set; }

            public AnimationChainBuilderComponent() {
                Animations = new List<IAnimation>();
            }

            public T WithAnimation(IAnimation animation) {
                if (animation == null) {
                    throw new ArgumentNullException("Animation cannot be null");
                }

                VerifyBuildingState();
                Animations.Add(animation);

                return this as T;
            }
        }

        /// <summary>
        /// Public class that contains method to build target class instance.
        /// </summary>
        public class AnimationChainBuilder : AnimationChainBuilderComponent<AnimationChainBuilder>, IBuilder<AnimationChain> {
            public AnimationChainBuilder() { }

            public AnimationChain Build() {
                VerifyBuildingState();
                SetBuildingComplete();

                return new AnimationChain(this);
            }
        }
    }
}
