﻿using DEDVisualizerRedux.Rewrite.Interfaces.Animation;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace DEDVisualizerRedux.Rewrite.Classes.Animation.Impl {

    /// <summary>
    /// Handles updating, playing, and removing all active UI animations
    /// </summary>
    class AnimationEngine {

        /// <summary>
        /// Singleton instance of the engine
        /// </summary>
        private static AnimationEngine INSTANCE;

        /// <summary>
        /// All animations that are active
        /// </summary>
        private List<IAnimation> animations;

        private AnimationEngine() {
            animations = new List<IAnimation>();
        }

        /// <summary>
        /// Retrieves or creates an instance of the AnimationEngine
        /// </summary>
        /// <returns>
        ///     The engine instance
        /// </returns>
        public AnimationEngine GetEngine() {
            if (INSTANCE == null) {
                INSTANCE = new AnimationEngine();
            }

            return INSTANCE;
        }

        /// <summary>
        /// Updates all animations present in the engine
        /// </summary>
        /// <param name="gameTime">
        ///     Updated GameTime
        /// </param>
        public void Update(GameTime gameTime) {
            foreach (IAnimation animation in animations) {
                animation.Play(gameTime);
                if (animation.IsFinished()) {
                    animations.Remove(animation);
                }
            }
        }

        /// <summary>
        /// Adds an animation to the engine
        /// </summary>
        /// <param name="animation">
        ///     The animation to add
        /// </param>
        public void AddAnimation(IAnimation animation) {
            if (!animations.Contains(animation)) {
                animations.Add(animation);
            }
        }
    }
}
