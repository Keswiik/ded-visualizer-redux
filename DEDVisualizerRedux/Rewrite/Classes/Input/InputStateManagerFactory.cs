﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Classes.Input {
    class InputStateManagerFactory {
        private static InputStateManager INSTANCE;

        public static InputStateManager GetInputStateManager() {
            if (INSTANCE == null) {
                INSTANCE = new InputStateManager();
            }

            return INSTANCE;
        }
    }
}
