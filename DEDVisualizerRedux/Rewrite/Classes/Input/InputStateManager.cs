﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using DEDVisualizerRedux.Rewrite.Interfaces.Input;
using DEDVisualizerRedux.Rewrite.Interfaces.Events;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;
using DEDVisualizerRedux.Rewrite.Classes.Events.Constants;
using DEDVisualizerRedux.Rewrite.Classes.Events;
using DEDVisualizerRedux.Rewrite.Classes.Input.Constants;
using DEDVisualizerRedux.Rewrite.Classes.UI.Impl.Canvas;
using System.Diagnostics;

namespace DEDVisualizerRedux.Rewrite.Classes.Input {
    class InputStateManager : IInputStateManager {
        private MouseState mouseState;
        private KeyboardState keyboardState;
        private IEnumerable<IEventData> focusedEventQueue;
        private IEnumerable<IEventData> globalEventQueue;
        private IEnumerable<IEventData> mouseEventQueue;
        private bool mouseDragging;

        internal InputStateManager() {
            focusedEventQueue = new List<IEventData>();
            globalEventQueue = new List<IEventData>();
            mouseEventQueue = new List<IEventData>();
            mouseDragging = false;
        }

        public MouseState MouseState {
            get { return mouseState; }
            set {
                if (value == null) {
                    throw new ArgumentNullException("Mouse state cannot be null");
                }
                mouseState = value;
            }
        }

        public KeyboardState KeyboardState {
            get { return keyboardState; }
            set {
                if (value == null) {
                    throw new ArgumentNullException("Keyboard state cannot be null");
                }

                keyboardState = value;
            }
        }

        public IEnumerable<IEventData> FocusedEventQueue {
            get { return focusedEventQueue; }
        }

        public IEnumerable<IEventData> GlobalEventQueue {
            get { return globalEventQueue; }
        }

        public IEnumerable<IEventData> MouseEventQueue {
            get { return mouseEventQueue; }
        }

        public void GenerateEvents (IUIComponent focusedComponent) {
            GenerateEvents(focusedComponent, Mouse.GetState(), Keyboard.GetState());
        }

        public void GenerateEvents (IUIComponent focusedComponent, MouseState mouseState, KeyboardState keyboardState) {
            globalEventQueue = GenerateKeyboardEvents(keyboardState);
            mouseEventQueue = GenerateMouseEvents(mouseState);
        }

        public IEnumerable<IEventData> GenerateKeyboardEvents(KeyboardState newKeyboardState) {
            List<IEventData> events = new List<IEventData>();
            Keys[] pressedKeys = newKeyboardState.GetPressedKeys();
            Keys[] lastPressedKeys = keyboardState.GetPressedKeys();

            events.AddRange(CreateKeyEvents(lastPressedKeys.Except(pressedKeys),
                EventType.BUTTON_RELEASED));
            events.AddRange(CreateKeyEvents(pressedKeys.Except(lastPressedKeys),
                EventType.BUTTON_PRESSED));
            events.AddRange(CreateKeyEvents(pressedKeys.Intersect(lastPressedKeys), 
                EventType.BUTTON_HELD));

            return events;
        }

        public IEnumerable<IEventData> GenerateMouseEvents(MouseState newMouseState) {
            List<IEventData> events = new List<IEventData>();

            IEventData tempEvent = CheckMouseDrag(newMouseState);
            if (tempEvent != null) {
                events.Add(tempEvent);
            }

            tempEvent = CheckMouseButtonState(mouseState.LeftButton, newMouseState.LeftButton, MouseButton.LeftButton);
            if (tempEvent != null && !mouseDragging) {
                if (tempEvent.EventType == EventType.MOUSE_BUTTON_RELEASED) {
                    events.Add(new EventData(EventType.SELECTED));
                }
                events.Add(tempEvent);
            }

            tempEvent = CheckMouseButtonState(mouseState.RightButton, newMouseState.RightButton, MouseButton.RightButton);
            if (tempEvent != null && !mouseDragging) {
                events.Add(tempEvent);
            }

            //foreach(IEventData eventData in events) {
            //    Debug.WriteLine(eventData.EventType);
            //}

            return events;
        }

        private IEventData CheckMouseDrag(MouseState newMouseState) {
            IEventData leftButtonState = CheckMouseButtonState(mouseState.LeftButton, newMouseState.LeftButton, MouseButton.LeftButton);
            if (leftButtonState == null) {
                return null;
            }

            if (leftButtonState.EventType == EventType.MOUSE_BUTTON_HELD) {
                if (mouseState.X != newMouseState.X || mouseState.Y != newMouseState.Y) {
                    EventType eventType = (mouseDragging) ? EventType.DRAG : EventType.DRAG_START;
                    mouseDragging = true;
                    EventData eventData = new EventData(eventType);
                    eventData.AddEntry("mouseLocation", new Vector2(mouseState.X, mouseState.Y));
                    return eventData;
                }
            } else if (leftButtonState.EventType == EventType.MOUSE_BUTTON_RELEASED && mouseDragging) {
                EventType eventType = EventType.DRAG_END;
                mouseDragging = false;
                EventData eventData = new EventData(eventType);
                eventData.AddEntry("mouseLocation", new Vector2(newMouseState.X, newMouseState.Y));
                return eventData;
            }

            return null;
        }

        private IEventData CheckMouseButtonState(ButtonState oldState, ButtonState newState, MouseButton button) {
            IEventData eventData = null;
            if (oldState == ButtonState.Pressed && newState == ButtonState.Released) {
                eventData = new EventData(EventType.MOUSE_BUTTON_RELEASED);
            } else if (oldState == ButtonState.Pressed && newState == ButtonState.Pressed) {
                eventData = new EventData(EventType.MOUSE_BUTTON_HELD);
            } else if (oldState == ButtonState.Released && newState == ButtonState.Pressed) {
                eventData = new EventData(EventType.MOUSE_BUTTON_PRESSED);
            }
            
            if (eventData != null) {
                eventData.AddEntry("button", button);
            }

            return eventData;
        }

        private IEnumerable<IEventData> CreateKeyEvents (IEnumerable<Keys> keys, EventType eventType) {
            List<IEventData> events = new List<IEventData>();
            foreach (Keys key in keys) {
                IEventData eventData = new EventData(eventType);
                eventData.AddEntry("button", key);
                events.Add(eventData);
            }

            return events;
        }

        public IUIComponent FindMousedOverComponent (IUIComponent focusedComponent, MouseState mouseState) {
            List<int> keys = CanvasRegistry.GetLevels();
            IUIComponent mousedOverComponent = null;
            foreach (int level in keys) {
                foreach(ICanvas canvas in CanvasRegistry.Registry[level]) {
                    mousedOverComponent = FindMousedOverComponent(canvas, focusedComponent, mouseState);
                    if (mousedOverComponent != null) {
                        break;
                    }
                }
            }

            return mousedOverComponent;
        }

        public IUIComponent FindMousedOverComponent(ICanvas canvas, IUIComponent focusedComponent, MouseState mouseState) {
            List<int> levels = canvas.RegisteredLevels;
            IUIComponent mousedOverComponent = null;
            foreach (int level in levels) {
                List<IUIComponent> children = canvas.GetChildrenWithLevel(level);
                mousedOverComponent = FindMousedOverComponent(children, mouseState);
                if (mousedOverComponent != null) {
                    break;
                }
            }

            return mousedOverComponent;
        }

        public IUIComponent FindMousedOverComponent (List<IUIComponent> allComponennts, MouseState mouseState) {
            IUIComponent mousedOverComponent = null;
            foreach (IUIComponent component in allComponennts) {
                if (component.Active && component.Size.Contains(mouseState.X, mouseState.Y)) {
                    if (component is IUIPanel) {
                        mousedOverComponent = FindMousedOverComponent(((IUIPanel) component).Children, mouseState);
                    } else {
                        mousedOverComponent = component;
                    }
                    break;
                }
            }

            return mousedOverComponent;
        }

        private IUIPanel FindActiveParent(IUIComponent disabledComponent) {

            IUIPanel activeParent = disabledComponent.Parent;
            while (activeParent.Parent != null && !activeParent.Parent.Active) {
                activeParent = activeParent.Parent;
            }

            return disabledComponent.Parent;
        }

        private IUIComponent FindActiveChild(IUIPanel parent, MouseState mouseState) {
            IUIComponent activeChild = (IUIComponent) parent;
            foreach (IUIComponent child in parent.Children) {
                if (child.Active && child.Size.Contains(mouseState.X, mouseState.Y)) {
                    if (child is IUIPanel) {
                        activeChild = FindActiveChild((IUIPanel) child, mouseState);
                    } else {
                        activeChild = child;
                    }
                    break;
                }
            }

            return activeChild;
        }
    }
}
