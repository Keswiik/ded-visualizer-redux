﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Classes.Builder {
    class ExpressionBuilder {
        internal bool buildingComplete;
        public bool BuildingComplete {
            get { return buildingComplete; }
        }

        public void SetBuildingComplete () {
            buildingComplete = true;
        }

        public void VerifyBuildingState () {
            if (buildingComplete) {
                throw new System.InvalidOperationException();
            }
        }
    }
}
