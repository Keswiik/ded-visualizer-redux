﻿using Npgsql;
using System;
using System.Collections.Generic;
using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Classes.Persistence;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes;

namespace DEDVisualizerRedux.Rewrite.Classes.Playlist {

    [Table("playlists")]
    class Playlist : IPlaylist {
        internal int id;
        internal string name;
        internal string path;
        internal List<ISong> songs;

        public Playlist(IPlaylistBuilderComponent builder) {
            Songs = builder.Songs;
            Name = builder.Name;
            Path = builder.Path;
        }

        [Column("id", NpgsqlTypes.NpgsqlDbType.Integer, PrimaryKey = true)]
        public int Id {
            get { return id; }
            set { id = value; }
        }

        [Column("playlist_name", NpgsqlTypes.NpgsqlDbType.Varchar)]
        public string Name {
            get { return name; }
            set {
                name = value ?? throw new ArgumentNullException("Playlist name cannot be null");
            }
        }

        [Column("path", NpgsqlTypes.NpgsqlDbType.Text)]
        public string Path {
            get { return Path; }
            set { path = value; }
        }

        public List<ISong> Songs {
            get { return songs; }
            set {
                songs = value ?? throw new ArgumentNullException("Songs cannot be null");
            }
        }

        public List<ISong> Shuffle() {
            Random rand = new Random();
            List<ISong> copy = new List<ISong>(songs);
            for (int i = copy.Count - 1; i >= 0; i--) {
                int loc = rand.Next(i);
                ISong temp = copy[i];
                copy[i] = copy[loc];
                copy[loc] = temp;
            }

            return copy;
        }

        internal interface IPlaylistBuilderComponent {
            int Id { get; }
            string Name { get; }
            string Path { get; }
            List<ISong> Songs { get; }
        }

        internal class PlaylistBuilderComponent<T> : ExpressionBuilder, IPlaylistBuilderComponent where T: ExpressionBuilder {
            public int Id { get; internal set; }
            public string Name { get; internal set; }
            public string Path { get; internal set; }
            public List<ISong> Songs { get; internal set; }

            public T WithName(string name) {
                VerifyBuildingState();
                Name = name;

                return this as T;
            }

            public T WithPath(string path) {
                VerifyBuildingState();
                Path = path;

                return this as T;
            }

            public T WithSongs(List<ISong> songs) {
                VerifyBuildingState();
                Songs = songs;

                return this as T;
            }

            public T WithSong(ISong song) {
                VerifyBuildingState();

                if (Songs == null) {
                    Songs = new List<ISong>();
                }
                Songs.Add(song);

                return this as T;
            }

            internal T WithId(int id) {
                VerifyBuildingState();
                Id = id;

                return this as T;
            }

            public T CopyFromReader(NpgsqlDataReader reader) {
                int id = (int)reader["id"];
                string playlistName = reader["playlist_name"] as string;
                string path = reader["path"] as string;
                List<ISong> songs = DatabaseDataFactory.FetchSongsForPlaylist(id);

                WithId(id);
                WithName(playlistName);
                WithPath(path);
                WithSongs(songs);

                return this as T;
            }
        }

        public class PlaylistBuilder : PlaylistBuilderComponent<PlaylistBuilder>, IBuilder<Playlist> {
            public PlaylistBuilder() {
                Id = -1;
            }

            public Playlist Build() {
                return new Playlist(this);
            }
        }
    }
}
