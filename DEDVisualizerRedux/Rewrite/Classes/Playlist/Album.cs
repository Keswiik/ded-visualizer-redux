﻿using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using DEDVisualizerRedux.Rewrite.Classes.Builder;
using System;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using Npgsql;
using DEDVisualizerRedux.Rewrite.Classes.Persistence;
using DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes;

namespace DEDVisualizerRedux.Rewrite.Classes.Playlist {

    [Table("albums")]
    class Album : IAlbum {
        internal string title;
        internal IArtist artist;
        internal int year;
        internal int numTracks;
        internal int id;

        public Album(IAlbumBuilderComponent builder) {
            Title = builder.Title;
            Artist = builder.Artist;
            Year = builder.Year;
            NumTracks = builder.NumTracks;
        }

        [Column("id", NpgsqlTypes.NpgsqlDbType.Integer, PrimaryKey = true)]
        public int Id {
            get { return id; }
            set { id = value; }
        }

        [Column("artistID", NpgsqlTypes.NpgsqlDbType.Integer, ForeignKey = true)]
        public IArtist Artist {
            get { return artist; }
            set { artist = value; }
        }

        [Column("album_name", NpgsqlTypes.NpgsqlDbType.Varchar)]
        public string Title {
            get { return title; }
            set {
                title = value;
            }
        }

        [Column("year", NpgsqlTypes.NpgsqlDbType.Integer)]
        public int Year {
            get { return year; }
            set {
                year = value;
            }
        }

        [Column("numTracks", NpgsqlTypes.NpgsqlDbType.Integer)]
        public int NumTracks {
            get { return numTracks; }
            set {
                if (value < 0) {
                    throw new ArgumentOutOfRangeException("Album cannot have negative tracks");
                }

                numTracks = value;
            }
        }

        internal interface IAlbumBuilderComponent {
            string Title { get; }
            IArtist Artist { get; }
            int Year { get; }
            int NumTracks { get; }
            int Id { get; }
        }

        internal class AlbumBuilderComponent<T> : ExpressionBuilder, IAlbumBuilderComponent where T : ExpressionBuilder {
            public string Title { get; internal set; }
            public IArtist Artist { get; internal set; }
            public int Year { get; internal set; }
            public int NumTracks { get; internal set; }
            public int Id { get; internal set; }

            public T WithTitle(string title) {
                VerifyBuildingState();
                Title = title;

                return this as T;
            }

            public T WithArtist(IArtist artist) {
                VerifyBuildingState();
                Artist = artist;

                return this as T;
            }

            public T WithYear(int year) {
                VerifyBuildingState();
                Year = year;

                return this as T;
            }

            public T WithNumTracks(int numTracks) {
                VerifyBuildingState();
                NumTracks = numTracks;

                return this as T;
            }

            internal T WithId(int id) {
                VerifyBuildingState();
                Id = id;

                return this as T;
            }

            public T CopyFromReader(NpgsqlDataReader reader) {
                int id = reader["id"] == null ? -1 : (int)reader["id"];
                int year = reader["year"] == null ? -1 : (int)reader["year"];
                int numTracks = reader["numTracks"] == null ? -1 : (int)reader["numTracks"];
                int artistID = reader["artistID"] == null ? -1 : (int)reader["artistID"];
                IArtist artistName = DatabaseDataFactory.FetchArtist(artistID);
                string name = reader["album_name"] as string;

                WithId(id);
                WithYear(year);
                WithNumTracks(numTracks);
                WithArtist(artistName);
                WithTitle(name);

                return this as T;
            }
        }

        public class AlbumBuilder : AlbumBuilderComponent<AlbumBuilder>, IBuilder<Album> {
            public AlbumBuilder() {
                Id = -1;
            }

            public Album Build() {
                return new Album(this);
            }
        }
    }
}
