﻿using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using Npgsql;
using System;

namespace DEDVisualizerRedux.Rewrite.Classes.Playlist {

    [Table("genres")]
    class Genre : IGenre {
        internal int id;
        internal string text;

        public Genre(IGenreBuilderComponent builder) {
            Id = builder.Id;
            Text = builder.Text;
        }

        [Column("id", NpgsqlTypes.NpgsqlDbType.Integer, PrimaryKey = true)]
        public int Id {
            get { return id; }
            set { id = value; }
        }

        [Column("genre_name", NpgsqlTypes.NpgsqlDbType.Text)]
        public string Text {
            get { return text; }
            set {
                text = value ?? throw new ArgumentNullException("Genre text cannot be null");
            }
        }

        public override bool Equals(object obj) {
            if (obj == null) {
                return false;
            } else if (!(obj is Genre)) {
                return false;
            } else {
                Genre genre = obj as Genre;
                if (genre.Id != Id) {
                    return false;
                } else if (genre.Text != Text) {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode() {
            return Text.GetHashCode();
        }

        internal interface IGenreBuilderComponent {
            int Id { get; }
            string Text { get; }
        }

        internal class GenreBuilderComponent<T> : ExpressionBuilder, IGenreBuilderComponent where T: ExpressionBuilder {
            public int Id { get; internal set; }
            public string Text { get; internal set; }

            public T WithText(string text) {
                VerifyBuildingState();
                Text = text;

                return this as T;
            }

            internal T WithId(int id) {
                VerifyBuildingState();
                Id = id;

                return this as T;
            }

            internal T CopyFromReader(NpgsqlDataReader reader) {
                VerifyBuildingState();
                int id = reader["id"] == null ? -1 : (int)reader["id"];
                string name = reader["genre_name"] as string;

                WithId(id);
                WithText(name);

                return this as T;
            }
        }

        public class GenreBuilder : GenreBuilderComponent<GenreBuilder>, IBuilder<Genre> {
            public GenreBuilder() {
                Id = -1;
            }

            public Genre Build() {
                return new Genre(this);
            }
        }
    }
}
