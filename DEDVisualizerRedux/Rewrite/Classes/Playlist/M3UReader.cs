﻿using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using System;
using System.Collections.Generic;
using System.IO;

namespace DEDVisualizerRedux.Rewrite.Classes.Playlist {
    class M3UReader : IPlaylistDeserializer {
        internal bool isExtendedFormat;
        internal static readonly string EXTENDED_HEADER = "#EXTM3U";
        internal static readonly string EXTENDED_INFO = "#EXTINF";
        internal StreamReader streamReader;
        internal string filePath;
        internal List<ISong> failedSongs;
        internal IPlaylistDeserializerCallback callback;

        public M3UReader(string filePath, IPlaylistDeserializerCallback callback) {
            this.filePath = filePath;
            this.callback = callback;
            failedSongs = new List<ISong>();
        }

        public IPlaylist ReadPlaylist() {
            streamReader = new StreamReader(filePath);
            CheckIfExtended();
            throw new NotImplementedException();
        }

        internal void CheckIfExtended() {
            while (!streamReader.EndOfStream) {
                if (streamReader.ReadLine().Trim() == EXTENDED_HEADER) {
                    isExtendedFormat = true;
                    break;
                }
            }

            streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
        }

        internal ISong ReadNextSong(StreamReader streamReader) {
            if (streamReader.EndOfStream) {
                return null;
            }

            string data = FindNextValidLine(streamReader);
            string location = FindNextValidLine(streamReader);

            if (data == null || location == null || !data.StartsWith(EXTENDED_INFO)) {
                throw new Exception("Supplied file does not match M3U format");
            }

            try {
                TagLib.File file = TagLib.File.Create(location);
            } catch (Exception e) {
            }

            return null;
        }

        internal string FindNextValidLine(StreamReader streamReader) {
            if (streamReader.EndOfStream) {
                return null;
            }

            string nextLine = streamReader.ReadLine();
            while (!streamReader.EndOfStream && nextLine.Trim() == "") {
                nextLine = streamReader.ReadLine().Trim();
            }

            return (nextLine == "") ? null : nextLine;
        }
    }
}
