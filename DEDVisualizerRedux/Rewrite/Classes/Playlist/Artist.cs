﻿using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using Npgsql;

namespace DEDVisualizerRedux.Rewrite.Classes.Playlist {

    [Table("artists")]
    class Artist : IArtist {
        internal int id;
        internal string name;

        public Artist(IArtistBuilderComponent builder) {
            Id = builder.Id;
            Name = builder.Name;
        }

        [Column("id", NpgsqlTypes.NpgsqlDbType.Integer, PrimaryKey = true)]
        public int Id {
            get { return id; }
            set { id = value; }
        }

        [Column("artist_name", NpgsqlTypes.NpgsqlDbType.Varchar)]
        public string Name {
            get { return name; }
            set { name = value; }
        }
        
        internal interface IArtistBuilderComponent {
            int Id { get; }
            string Name { get; }
        }

        internal class ArtistBuilderComponent<T> : ExpressionBuilder, IArtistBuilderComponent where T: ExpressionBuilder {
            public int Id { get; internal set; }
            public string Name { get; internal set; }

            public T WithName(string name) {
                VerifyBuildingState();
                Name = name;

                return this as T;
            }

            internal T WithId(int id) {
                VerifyBuildingState();
                Id = id;

                return this as T;
            }

            public T CopyFromReader(NpgsqlDataReader reader) {
                VerifyBuildingState();
                int id = reader["id"] == null ? -1 : (int)reader["id"];
                string name = reader["artist_name"] as string;

                WithId(id);
                WithName(name);

                return this as T;
            }
        }

        public class ArtistBuilder : ArtistBuilderComponent<ArtistBuilder>, IBuilder<Artist> {
            public ArtistBuilder() {
                Id = -1;
            }

            public Artist Build() {
                return new Artist(this);
            }
        }
    }
}
