﻿using DEDVisualizerRedux.Rewrite.Classes.Builder;
using DEDVisualizerRedux.Rewrite.Classes.Persistence;
using DEDVisualizerRedux.Rewrite.Interfaces.Builder;
using DEDVisualizerRedux.Rewrite.Interfaces.Playlist;
using DEDVisualizerRedux.Rewrite.Classes.Persistence.Attributes;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;

namespace DEDVisualizerRedux.Rewrite.Classes.Playlist {

    [Table("songs")]
    class Song : ISong {
        internal string title;
        internal string path;
        internal int id;
        internal List<IGenre> genres;
        internal List<IArtist> artists;
        internal float duration;
        internal IAlbum album;

        public Song(ISongBuilderComponent builder) {
            title = builder.Title;
            path = builder.Path;
            genres = builder.Genres;
            artists = builder.Artists;
            duration = builder.Duration;
            album = builder.Album;
            id = builder.Id;
        }

        [Column("id", NpgsqlTypes.NpgsqlDbType.Integer, PrimaryKey = true)]
        public int Id {
            get { return id; }
            set { id = value; }
        }

        [Column("albumID", NpgsqlTypes.NpgsqlDbType.Integer, ForeignKey = true)]
        public IAlbum Album {
            get { return album; }
            set { album = value; }
        }

        [Column("duration", NpgsqlTypes.NpgsqlDbType.Integer)]
        public float Duration {
            get { return duration; }
            set {
                if (duration < 0) {
                    throw new ArgumentOutOfRangeException("Invalid duration");
                }

                duration = value;
            }
        }

        [Column("song_name", NpgsqlTypes.NpgsqlDbType.Varchar)]
        public string Title {
            get { return title; }
            set {
                title = value ?? throw new ArgumentNullException("Title cannot be null");
            }
        }

        [Column("path", NpgsqlTypes.NpgsqlDbType.Text)]
        public string Path {
            get { return path; }
            set {
                title = value ?? throw new ArgumentNullException("Location cannot be null");
            }
        }

        public List<IGenre> Genres {
            get { return genres; }
            set { genres = value; }
        }

        public List<IArtist> Artists {
            get { return artists; }
            set { artists = value; }
        }

        public StreamReader GetStream() {
            return new StreamReader(path);
        }

        public void Save() {
            NpgsqlConnection conn = DatabaseManagerImpl.GetInstance().GetConnection("visualizer");
        }

        public override string ToString() {
            return String.Format("[SONG | {0}] {1} - {2}", Id, Title, Path);
        }

        internal interface ISongBuilderComponent {
            int Id { get; }
            string Title { get; }
            string Path { get; }
            List<IGenre> Genres { get; }
            List<IArtist> Artists { get; }
            float Duration { get; }
            IAlbum Album { get; }
        }

        internal class SongBuilderComponent<T> : ExpressionBuilder, ISongBuilderComponent where T: ExpressionBuilder {
            public int Id { get; internal set; }
            public string Title { get; internal set; }
            public string Path { get; internal set; }
            public List<IGenre> Genres { get; internal set; }
            public List<IArtist> Artists { get; internal set; }
            public float Duration { get; internal set; }
            public IAlbum Album { get; internal set; }

            public T WithTitle(string title) {
                VerifyBuildingState();
                Title = title;

                return this as T;
            }

            public T WithLocation(string path) {
                VerifyBuildingState();
                Path = path;

                return this as T;
            }

            public T WithDuration(float duration) {
                VerifyBuildingState();
                Duration = duration;

                return this as T;
            }

            public T WithGenres(List<IGenre> genres) {
                VerifyBuildingState();
                Genres = genres;

                return this as T;
            }

            public T WithGenre(IGenre genre) {
                VerifyBuildingState();
                if (Genres == null) {
                    Genres = new List<IGenre>();
                }
                Genres.Add(genre);

                return this as T;
            }

            public T WithArtists(List<IArtist> artists) {
                VerifyBuildingState();
                Artists = artists;

                return this as T;
            }

            public T WithArtist(IArtist artist) {
                VerifyBuildingState();
                if (Artists == null) {
                    Artists = new List<IArtist>();
                }
                Artists.Add(artist);

                return this as T;
            }

            public T WithAlbum(IAlbum album) {
                VerifyBuildingState();
                Album = album;

                return this as T;
            }

            internal T WithId(int id) {
                VerifyBuildingState();
                Id = id;

                return this as T;
            }

            public T CopyFromReader(NpgsqlDataReader reader) {
                int id = (int)reader["id"];
                string title = reader["song_name"] as string;
                string path = reader["path"] as string;
                int albumID = reader["albumID"] == DBNull.Value ? -1 : (int)reader["albumID"];
                int duration = reader["duration"] == DBNull.Value ? -1 : (int)reader["duration"];
                List<IGenre> genres = DatabaseDataFactory.FetchGenresForSong(id);
                List<IArtist> artists = DatabaseDataFactory.FetchArtistsForSong(id);
                IAlbum album = DatabaseDataFactory.FetchAlbum(albumID);

                WithId(id);
                WithDuration(duration);
                WithTitle(title);
                WithLocation(path);
                WithArtists(artists);
                WithGenres(genres);
                WithAlbum(album);

                return this as T;
            }
        }

        public class SongBuilder : SongBuilderComponent<SongBuilder>, IBuilder<Song> {
            public SongBuilder() {
                Id = -1;
                Genres = new List<IGenre>();
                Artists = new List<IArtist>();
            }

            public Song Build() {
                return new Song(this);
            }
        }
    }
}
