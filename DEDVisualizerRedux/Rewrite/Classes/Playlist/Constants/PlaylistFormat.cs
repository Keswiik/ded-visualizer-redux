﻿using System;

namespace DEDVisualizerRedux.Rewrite.Classes.Playlist.Constants {
    enum PlaylistFormat {
        M3U,
        M3U8,
        PLS,
        UNKNOWN
    }

    public class PlaylistFormatConverter {
        static PlaylistFormat GetFormatForExtension(string fileExtension) {
            foreach (PlaylistFormat format in Enum.GetValues(typeof(PlaylistFormat))) {
                if (format.ToString().ToLower() == fileExtension.ToLower()) {
                    return format;
                }
            }

            return PlaylistFormat.UNKNOWN;
        }
    }
}
