﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using Microsoft.Xna.Framework;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Animation {
    interface IAnimation {
        void Play (GameTime gametime);

        bool IsFinished ();
    }
}
