﻿
namespace DEDVisualizerRedux.Rewrite.Interfaces.EventListeners {

    /// <summary>
    /// A generic interface to be implemented by all event listeners.
    /// </summary>
    interface IEventListener {
    }
}
