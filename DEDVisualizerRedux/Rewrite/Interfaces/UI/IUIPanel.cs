﻿using System.Collections.Generic;

namespace DEDVisualizerRedux.Rewrite.Interfaces.UI
{
    /// <summary>
    /// Outlines basic functionality of any UI panel (mainly just handling children)
    /// </summary>
    interface IUIPanel : IComponent
    {

        /// <summary>
        /// The children present within this panel
        /// </summary>
        List<IUIComponent> Children { get; }

        /// <summary>
        /// Adds a child to this panel
        /// </summary>
        /// <param name="uiComponent">
        ///     The child to add
        /// </param>
        void AddChild(IUIComponent uiComponent);

        /// <summary>
        /// Removes a child from this panel
        /// </summary>
        /// <param name="uiComponent">
        ///     The child to remove
        /// </param>
        /// <returns>
        ///     true if the child was successfully removed, otherwise false
        /// </returns>
        bool RemoveChild(IUIComponent uiComponent);
    }
}
