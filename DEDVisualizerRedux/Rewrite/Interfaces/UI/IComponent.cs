﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Interfaces.UI {

    /// <summary>
    /// Represents the visual aspects of a UI component
    /// </summary>
    interface IComponent {

        /// <summary>
        ///     A <code>Rectangle</code> represending the size and location of the component
        /// </summary>
        Rectangle Size {
            get;
            set;
        }

        /// <summary>
        ///     Whether or not the component open to user interaction
        /// </summary>
        bool Enabled {
            get;
            set;
        }

        /// <summary>
        ///     Whether or not the component is active in the scene.
        ///     Inactive components will not be drawn, updated, or receive events.
        /// </summary>
        bool Active {
            get;
            set;
        }

        /// <summary>
        ///     The texture to use as this component's background.
        /// </summary>
        Texture2D Background {
            get;
            set;
        }

        /// <summary>
        ///     Minimum space left around the edges of the component
        /// </summary>
        Point Padding {
            get;
            set;
        }

        /// <summary>
        ///     The parent of this component.
        ///     The top-level rendering component will have no parent.
        /// </summary>
        IUIPanel Parent {
            get;
        }

        /// <summary>
        ///     The level/layer which this component is located in
        /// </summary>
        int Level {
            get;
            set;
        }

        /// <summary>
        ///     Used to update component state.
        /// </summary>
        void Update ();

        /// <summary>
        ///     Draws the component on the screen.
        /// </summary>
        void Draw (SpriteBatch spriteBatch);

        /// <summary>
        ///     Moves the component on the screen.
        /// </summary>
        /// <param name="delta">
        ///     The units (and direction) in which to move the component
        /// </param>
        void Move (Point delta);

        /// <summary>
        ///     Move the component to an absolute position on the screen
        /// </summary>
        /// <param name="position">
        ///     The new position for the component
        /// </param>
        void SetPosition (Point position);

        //void LayoutChildren();
    }
}
