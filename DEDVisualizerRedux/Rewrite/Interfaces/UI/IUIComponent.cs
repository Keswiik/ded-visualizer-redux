﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DEDVisualizerRedux.Rewrite.Interfaces.Events;
using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;

namespace DEDVisualizerRedux.Rewrite.Interfaces.UI {

    /// <summary>
    /// Aggregate interface represending a standard UI component
    /// </summary>
    interface IUIComponent : IComponent, IEventTarget {
    }
}
