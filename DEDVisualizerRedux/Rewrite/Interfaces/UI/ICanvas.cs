﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace DEDVisualizerRedux.Rewrite.Interfaces.UI {

    /// <summary>
    /// An interface to represent the basic functionality of a canvas
    /// </summary>
    interface ICanvas : IComponent, IUIPanel {

        /// <summary>
        /// The game the canvas is tied to
        /// </summary>
        Game Game {
            get;
        }

        /// <summary>
        /// All known UI layers this canvas contains
        /// </summary>
        List<int> RegisteredLevels {
            get;
        }


        /// <summary>
        /// Retrieves all children with the specified level/layer
        /// </summary>
        /// <param name="level">
        ///     The level to find children of
        /// </param>
        /// <returns>
        ///     A list containing all children in the matching level, or an empty list if nothing was found
        /// </returns>
        List<IUIComponent> GetChildrenWithLevel (int level);
    }
}
