﻿using DEDVisualizerRedux.Rewrite.Interfaces.EventListeners;
using DEDVisualizerRedux.Rewrite.Interfaces.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Interfaces.UI {
    delegate void EventExecutor<T> (T listener, IEventData eventData) where T : IEventListener;

    /// <summary>
    /// The basic functionality to be provided by components that can be the target of events
    /// </summary>
    interface IEventTarget {

        /// <summary>
        ///     The list of listeners registered to this component.
        /// </summary>
        List<IEventListener> EventListeners {
            get;
        }

        /// <summary>
        ///     Whether or not the component intercepts all global events when focused.
        /// </summary>
        bool InterceptsGlobalEvents {
            get;
            set;
        }

        /// <summary>
        ///     Adds an event listener to the component
        /// </summary>
        /// <param name="listener">
        ///     The listener to add
        /// </param>
        void AddListener (IEventListener listener);

        /// <summary>
        ///     Removes a listener from the component
        /// </summary>
        /// <param name="listener">
        ///     The listener to remove
        /// </param>
        /// <returns>
        ///     <code>true</code> if the listener was removed, otherwise <code>false</code>
        /// </returns>
        bool RemoveListener (IEventListener listener);

        /// <summary>
        ///     A generic event that routes all events to their respective handlers
        /// </summary>
        /// <param name="eventData">
        ///     The data for the event
        /// </param>
        void OnEvent (IEventData eventData);

        /// <summary>
        ///     A typed method that fires events
        /// </summary>
        /// <typeparam name="T">
        ///     The type of <code>IEventListener</code> used by the event
        /// </typeparam>
        /// <param name="eventData">
        ///     The data for the event
        /// </param>
        /// <param name="eventExecutor">
        ///     A method which takes a typed <code>IEventListener</code> and <code>IEventData</code> 
        ///     and calls the appropriate listener methods.
        /// </param>
        void FireEvent<T> (IEventData eventData, EventExecutor<T> eventExecutor) where T : IEventListener;
    }
}
