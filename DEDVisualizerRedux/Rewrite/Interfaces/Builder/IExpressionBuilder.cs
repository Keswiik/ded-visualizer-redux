﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Builder {

    /// <summary>
    /// Outlines logic of any expression-based builder
    /// </summary>
    interface IExpressionBuilder {

        /// <summary>
        /// Whether or not the builder has instantiated an object before
        /// </summary>
        bool BuildingComplete {
            get;
        }

        /// <summary>
        /// Checks to make sure builder state is safe
        /// </summary>
        void VerifyBuildingState ();

        /// <summary>
        /// Sets BuildingComplete to true
        /// </summary>
        void SetBuildingComplete ();
    }
}
