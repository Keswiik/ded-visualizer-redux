﻿namespace DEDVisualizerRedux.Rewrite.Interfaces.Builder {

    /// <summary>
    /// Basic functionality of a builder
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IBuilder<T> {
        T Build ();
    }
}
