﻿using Npgsql;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Persistence {

    /// <summary>
    ///     Interface for all query builders to implement.
    /// </summary>
    interface IQueryBuilder {

        /// <summary>
        ///     Builds a query using some form of data source (to be determined by implementations).
        /// </summary>
        /// <returns>A command with a dynamically generated query string and parameters</returns>
        NpgsqlCommand Build();
    }
}
