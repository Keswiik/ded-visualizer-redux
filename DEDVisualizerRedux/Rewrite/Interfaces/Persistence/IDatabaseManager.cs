﻿using Npgsql;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Persistence {
    interface IDatabaseManager {
        void Initialize();
    }
}
