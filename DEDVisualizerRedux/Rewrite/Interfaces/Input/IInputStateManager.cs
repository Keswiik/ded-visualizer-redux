﻿using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using DEDVisualizerRedux.Rewrite.Interfaces.Events;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Input {

    /// <summary>
    /// An interface for classes designed to manage input devices and generate input events
    /// </summary>
    interface IInputStateManager {

        /// <summary>
        /// The state of the mouse during the last update frame
        /// </summary>
        MouseState MouseState {
            get;
            set;
        }

        /// <summary>
        /// The state of the keyboard duringt he last update frame
        /// </summary>
        KeyboardState KeyboardState {
            get;
            set;
        }

        /// <summary>
        /// A queue for all mouse events
        /// </summary>
        IEnumerable<IEventData> MouseEventQueue {
            get;
        }

        /// <summary>
        /// A queue for all global events (namely keypresses)
        /// </summary>
        IEnumerable<IEventData> GlobalEventQueue {
            get;
        }

        /// <summary>
        /// A queue for focused events (selection, drag, double-click, etc)
        /// </summary>
        IEnumerable<IEventData> FocusedEventQueue {
            get;
        }

        /// <summary>
        /// Generates all mouse events
        /// </summary>
        /// <param name="mouseState">
        ///     The current state of the mouse
        /// </param>
        /// <returns>
        ///     An enumerable containing all new mouse events
        /// </returns>
        IEnumerable<IEventData> GenerateMouseEvents (MouseState mouseState);

        /// <summary>
        /// Generates all keyboard events
        /// </summary>
        /// <param name="keyboardState">
        ///     The current state of the keyboard
        /// </param>
        /// <returns>
        ///     An enumerable containing all new keyboard events
        /// </returns>
        IEnumerable<IEventData> GenerateKeyboardEvents (KeyboardState keyboardState);

        /// <summary>
        /// Attempts to find the currently moused over component, starting with the component that currently has focus
        /// </summary>
        /// <param name="focusedComponent">
        ///     The component that has focus
        /// </param>
        /// <param name="mouseState">
        ///     The current state of the mouse
        /// </param>
        /// <returns>
        ///     The moused over component if found, otherwise null.
        /// </returns>
        IUIComponent FindMousedOverComponent (IUIComponent focusedComponent, MouseState mouseState);

        /// <summary>
        /// Attempts to find the currently moused over component from a group of components, generally the children of a panel.
        /// If this is a canvas, it searches by active levels.
        /// </summary>
        /// <param name="allComponennts">
        ///     All components to check
        /// </param>
        /// <param name="mouseState">
        ///     The current state of the mouse
        /// </param>
        /// <returns>
        ///     The moused over component if found, otherwise null
        /// </returns>
        IUIComponent FindMousedOverComponent (List<IUIComponent> allComponennts, MouseState mouseState);

        /// <summary>
        /// Generates all events, retrieving updated mouse and keyboard states
        /// </summary>
        /// <param name="focusedComponent"></param>
        void GenerateEvents (IUIComponent focusedComponent);

        /// <summary>
        /// Generates all events with provided mouse and keyboard states
        /// </summary>
        /// <param name="focusedComponent"></param>
        /// <param name="mouseState"></param>
        /// <param name="keyboardState"></param>
        void GenerateEvents (IUIComponent focusedComponent, MouseState mouseState, KeyboardState keyboardState);
    }
}
