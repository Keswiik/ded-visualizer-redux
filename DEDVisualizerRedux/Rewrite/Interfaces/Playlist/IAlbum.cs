﻿namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface IAlbum {
        IArtist Artist { get; set; }
        string Title { get; set; }
        int Year { get; set; }
        int NumTracks { get; set; }
        int Id { get; }
    }
}
