﻿using DEDVisualizerRedux.Rewrite.Interfaces.Persistence;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface IArtist : IDatabaseEntity {
        int Id { get; }
        string Name { get; }
    }
}
