﻿using System.Collections.Generic;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface IPlaylist {
        string Name { get; set; }
        string Path { get; set; }
        List<ISong> Songs { get; set; }
        int Id { get; }

        List<ISong> Shuffle();
    }
}
