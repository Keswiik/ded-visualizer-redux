﻿namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface IPlaylistDeserializer {
        IPlaylist ReadPlaylist();
    }
}
