﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface IPlaylistSerializer {
        void SerializePlaylist(IPlaylist playlist);
    }
}
