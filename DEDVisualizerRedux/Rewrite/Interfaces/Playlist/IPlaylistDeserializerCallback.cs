﻿using System.Collections.Generic;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface IPlaylistDeserializerCallback {
        void OnFailedSongs(List<ISong> failedSongs);
        void OnFinishedReading(IPlaylist playlist);
    }
}
