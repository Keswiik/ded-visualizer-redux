﻿using DEDVisualizerRedux.Rewrite.Interfaces.Persistence;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface IGenre : IDatabaseEntity {
        int Id { get; set; }
        string Text { get; set; }
    }
}
