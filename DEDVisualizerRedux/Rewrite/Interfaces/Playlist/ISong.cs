﻿using DEDVisualizerRedux.Rewrite.Interfaces.Persistence;
using System.Collections.Generic;
using System.IO;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Playlist {
    interface ISong : IDatabaseEntity {
        string Title { get; set; }
        string Path { get; set; }
        float Duration { get; }
        int Id { get; set; }
        List<IGenre> Genres { get; set; }
        List<IArtist> Artists { get; set; }
        IAlbum Album { get; set; }

        StreamReader GetStream();
    }
}
