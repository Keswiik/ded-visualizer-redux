﻿using DEDVisualizerRedux.Rewrite.Classes.UI.Abstract;
using DEDVisualizerRedux.Rewrite.Interfaces.Input;
using DEDVisualizerRedux.Rewrite.Interfaces.UI;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Events {

    /// <summary>
    /// Acts as the central message dispatcher.
    /// Most events received through the InputStateManager
    /// </summary>
    interface IEventRegistry {

        /// <summary>
        /// The component that currently has focus
        /// </summary>
        AbstractUIComponent FocusedComponent {
            get;
            set;
        }

        /// <summary>
        /// The component the mouse cursor is currently over
        /// </summary>
        IUIComponent MousedOverComponent {
            get;
            set;
        }

        /// <summary>
        /// Dispatches all events in the event queue
        /// </summary>
        void DispatchEvents ();
        
        /// <summary>
        /// Updates events and dispatches them
        /// </summary>
        void Update ();
    }
}
