﻿using DEDVisualizerRedux.Rewrite.Classes.Events.Constants;
using System.Collections.Generic;

namespace DEDVisualizerRedux.Rewrite.Interfaces.Events {
    /// <summary>
    ///     An interface specifying the functionality of all event data containers
    /// </summary>
    interface IEventData {

        /// <summary>
        ///     A container for all objects to be passed with the event
        /// </summary>
        Dictionary<string, object> Data {
            get;
        }

        /// <summary>
        ///     An enum specifying the event type
        /// </summary>
        /// <see cref="EventType"/>
        EventType EventType {
            get;
            set;
        }

        /// <summary>
        ///     Fetches any value from the event data dictionary
        /// </summary>
        /// <typeparam name="T">
        ///     The type of the value to fetch
        /// </typeparam>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The value, after casting to T
        /// </returns>
        /// <exception cref="ArgumentException">
        ///     Thrown if the found value is not of type T
        /// </exception>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if either the found object or key is <code>null</code>
        /// </exception>
        T GetData<T> (string key);

        /// <summary>
        ///     Fetches an integer from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The <code>int</code> if found
        /// </returns>
        int GetInt (string key);

        /// <summary>
        ///     Fetches a long from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The <code>long</code> if found
        /// </returns>
        long GetLong (string key);

        /// <summary>
        ///     Fetches a short from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The <code>short</code> if found
        /// </returns>
        short GetShort (string key);

        /// <summary>
        ///     Fetches a float from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The <code>float</code> if found
        /// </returns>
        float GetFloat (string key);

        /// <summary>
        ///     Fetches a double from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The <code>double</code> if found
        /// </returns>
        double GetDouble (string key);

        /// <summary>
        ///     Fetches a char from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The <code>char</code> if found
        /// </returns>
        char GetChar (string key);

        /// <summary>
        ///     Fetches a string from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value to fetch
        /// </param>
        /// <returns>
        ///     The <code>string</code> if found
        /// </returns>
        string GetString (string key);

        /// <summary>
        ///     Adds an entry to the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name for the value. Cannot be null.
        /// </param>
        /// <param name="data">
        ///     The value to store. Cannot be null.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if either the key or data are <code>null</code>
        /// </exception>
        void AddEntry (string key, object data);

        /// <summary>
        ///     Removes a value from the event data dictionary
        /// </summary>
        /// <param name="key">
        ///     The name of the value being removed
        /// </param>
        /// <returns>
        ///     <code>true</code> if the value was removed, otherwise <code>false</code>
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the <code>key</code> is <code>null</code>
        /// </exception>
        bool RemoveEntry (string key);
    }
}
