﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Classes.UI
{
    enum EventType
    {
        DOUBLE_CLICK,
        DRAG_START,
        DRAG,
        DRAG_END,
        FOCUS_GAINED,
        FOCUS_LOST,
        HOVER_START,
        HOVER_END,
        MODIFIED,
        SELECTED,
        MOUSE_BUTTON_PRESSED,
        MOUSE_BUTTON_HELD,
        MOUSE_BUTTON_RELEASED,
        MOUSE_OVER,
        MOUSE_OFF,
        BUTTON_PRESSED,
        BUTTON_HELD,
        BUTTON_RELEASED
    }
}
