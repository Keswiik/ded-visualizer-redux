﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces;
using DEDVisualizerRedux.Interfaces.UI;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Classes.UI
{
    class EventRegistry : IEventRegistry
    {
        private AbstractUIComponent focusedComponent;
        private static EventRegistry ACTIVE_REGISTRY;
        private IInputStateManager stateManager;
        private IUIComponent mousedOverComponent;

        public delegate void GlobalEventHandler (IEventData eventData);
        public event GlobalEventHandler GlobalEvent;

        public AbstractUIComponent FocusedComponent {
            get { return focusedComponent; }
            set {
                if (value != focusedComponent) {
                    if (focusedComponent != null) {
                        focusedComponent.OnEvent(new EventData(EventType.FOCUS_LOST));
                    }
                    value.OnEvent(new EventData(EventType.FOCUS_GAINED));
                }

                focusedComponent = value;
            }
        }

        public static EventRegistry ActiveRegistry {
            get {
                if (ACTIVE_REGISTRY == null) {
                    ACTIVE_REGISTRY = new EventRegistry();
                }

                return ACTIVE_REGISTRY;
            }
        }

        public IInputStateManager StateManager {
            get { return stateManager; }
            set {
                stateManager = value ?? throw new ArgumentNullException("Input state manager cannot be null");
            }
        }

        public IUIComponent MousedOverComponent {
            get { return mousedOverComponent; }
            set {
                if (value != null && value != mousedOverComponent) {
                    value.OnEvent(new EventData(EventType.MOUSE_OVER));
                }
                if (mousedOverComponent != null) {
                    mousedOverComponent.OnEvent(new EventData(EventType.MOUSE_OFF));
                }

                mousedOverComponent = value; }
        }

        public void DispatchEvents() {
            if (mousedOverComponent != null) {
                foreach (IEventData eventData in stateManager.MouseEventQueue) {
                    MousedOverComponent.OnEvent(eventData);
                }
            }

            GlobalEventHandler handler;
            if (focusedComponent != null && focusedComponent.InterceptsGlobalEvents) {
                handler = focusedComponent.OnEvent;
            } else {
                handler = GlobalEvent;
            }

            foreach (IEventData eventData in stateManager.GlobalEventQueue) {
                handler(eventData);
            }

            if (focusedComponent != null) {
                foreach (IEventData eventData in stateManager.FocusedEventQueue) {
                    focusedComponent.OnEvent(eventData);
                }
            }
        }

        public void Update() {
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyboardState = Keyboard.GetState();
            stateManager.GenerateEvents(focusedComponent, mouseState, keyboardState);
            mousedOverComponent = stateManager.FindMousedOverComponent(focusedComponent, mouseState);
            DispatchEvents();
            stateManager.MouseState = mouseState;
            stateManager.KeyboardState = keyboardState;
        }
    }
}
