﻿using DEDVisualizerRedux.Classes.UI.EventListeners;
using DEDVisualizerRedux.Interfaces;
using DEDVisualizerRedux.Interfaces.EventListeners;
using DEDVisualizerRedux.Interfaces.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace DEDVisualizerRedux.Classes.UI.Abstract {
    class AbstractUIComponent : IUIComponent {
        private Rectangle size;
        private bool enabled;
        private bool active;
        bool interceptsGlobalEvents;
        private int level;
        private Texture2D background;
        private IUIComponent parent;
        private List<IEventListener> EVENT_LISTENERS;

        public AbstractUIComponent (IUIComponent parent, Rectangle size, Texture2D background) : this(parent, size, background, 0) {
        }

        public AbstractUIComponent (IUIComponent parent, Rectangle size, Texture2D background, int level) {
            this.parent = parent;
            Size = size;
            Background = background;
            EVENT_LISTENERS = new List<IEventListener>();
            enabled = true;
            active = true;
        }

        public int Level {
            get { return level; }
            set {
                Canvas.ComponentRemovedFromLevel(level);
                Canvas.ComponentAddToLevel(value);

                level = value;
            }
        }

        public bool InterceptsGlobalEvents {
            get { return interceptsGlobalEvents; }
            set { interceptsGlobalEvents = value; }
        }

        public Rectangle Size {
            get { return size; }
            set {
                if (value == null) {
                    throw new ArgumentNullException("Size cannot be null");
                } else if (value.Height <= 0 || value.Width <= 0) {
                    throw new ArgumentOutOfRangeException("Height and Width must be greater than 0");
                }

                size = value;
            }
        }

        public bool Enabled {
            get { return enabled; }
            set { enabled = value; }
        }

        public bool Active {
            get { return active; }
            set { active = value; }
        }

        public Texture2D Background {
            get { return background; }
            set {
                background = value ?? throw new ArgumentNullException("Background texture cannot be null");
            }
        }

        public List<IEventListener> EventListeners {
            get { return EVENT_LISTENERS; }
        }

        public IUIComponent Parent {
            get { return parent; }
        }

        public void Move (Point delta) {
            size.Offset(delta);
        }

        public void SetPosition (Point position) {
            size.Location = position;
        }

        public void AddListener (IEventListener listener) {
            if (!EVENT_LISTENERS.Contains(listener)) {
                EVENT_LISTENERS.Add(listener);
                if (listener is KeyListener) {
                    EventRegistry.ActiveRegistry.GlobalEvent += OnEvent;
                }
            }
        }

        public bool RemoveListener (IEventListener listener) {
            if (listener is KeyListener && EVENT_LISTENERS.Contains(listener)) {
                EventRegistry.ActiveRegistry.GlobalEvent -= OnEvent;
            }
            return EVENT_LISTENERS.Remove(listener);
        }

        public virtual void Draw (SpriteBatch spriteBatch) { }

        public virtual void Update () { }

        public void OnEvent (IEventData eventData) {
            switch (eventData.EventType) {
                case EventType.DRAG_START:
                    TakeFocus();
                    goto case EventType.DRAG;
                case EventType.DRAG:
                case EventType.DRAG_END:
                    FireEvent<DragListener>(eventData, FireDragEvent);
                    break;
                case EventType.DOUBLE_CLICK:
                    TakeFocus();
                    FireEvent<DoubleClickListener>(eventData, FireDoubleClickEvent);
                    break;
                case EventType.HOVER_START:
                case EventType.HOVER_END:
                    FireEvent<HoverListener>(eventData, FireHoverEvent);
                    break;
                case EventType.FOCUS_GAINED:
                case EventType.FOCUS_LOST:
                    FireEvent<FocusListener>(eventData, FireFocusEvent);
                    break;
                case EventType.MODIFIED:
                    FireEvent<ModifyListener>(eventData, FireModifyEvent);
                    break;
                case EventType.MOUSE_BUTTON_RELEASED:
                    TakeFocus();
                    FireEvent<SelectionListener>(eventData, FireSelectionEvent);
                    break;
                case EventType.BUTTON_PRESSED:
                case EventType.BUTTON_RELEASED:
                    FireEvent<KeyListener>(eventData, FireKeyEvent);
                    break;

            }
        }

        private void TakeFocus() {
            EventRegistry.ActiveRegistry.FocusedComponent = this;
        }

        public void FireEvent<T> (IEventData eventData, EventExecutor<T> eventExecutor) where T : IEventListener {
            foreach (IEventListener listener in EVENT_LISTENERS) {
                if (listener is T) {
                    eventExecutor((T) listener, eventData);
                }
            }
        }

        private void FireDragEvent (DragListener listener, IEventData eventData) {
            Vector2 mousePosition = eventData.GetData<Vector2>("mousePosition");
            switch (eventData.EventType) {
                case EventType.DRAG_START:
                    listener.OnDragStart(this, mousePosition);
                    break;
                case EventType.DRAG:
                    listener.OnDrag(this, mousePosition);
                    break;
                case EventType.DRAG_END:
                    listener.OnDragEnd(this, mousePosition);
                    break;
            }
        }

        private void FireDoubleClickEvent (DoubleClickListener listener, IEventData eventData) {
            listener.OnDoubleClick(this);
        }

        private void FireFocusEvent (FocusListener listener, IEventData eventData) {
            switch (eventData.EventType) {
                case EventType.FOCUS_GAINED:
                    listener.OnFocusGained(this);
                    break;
                case EventType.FOCUS_LOST:
                    listener.OnFocusLost(this);
                    break;
            }
        }

        private void FireHoverEvent (HoverListener listener, IEventData eventData) {
            switch (eventData.EventType) {
                case EventType.HOVER_START:
                    listener.OnHoverStart(this);
                    break;
                case EventType.HOVER_END:
                    listener.OnHoverEnd(this);
                    break;
            }
        }

        private void FireModifyEvent (ModifyListener listener, IEventData eventData) {
            listener.OnModify(this);
        }

        private void FireSelectionEvent (SelectionListener listener, IEventData eventData) {
            listener.OnSelection(this);
        }

        private void FireKeyEvent(KeyListener listener, IEventData eventData) {
            Keys key = eventData.GetData<Keys>("button");
            if (eventData.EventType == EventType.BUTTON_PRESSED) {
                listener.OnKeyPressed(this, key);
            } else if (eventData.EventType == EventType.BUTTON_RELEASED) {
                listener.OnKeyReleased(this, key);
            }
        }
    }
}
