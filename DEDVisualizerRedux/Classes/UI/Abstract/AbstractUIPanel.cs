﻿using DEDVisualizerRedux.Interfaces.UI;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DEDVisualizerRedux.Classes.UI.Abstract {
    class AbstractUIPanel : AbstractUIComponent, IUIPanel {
        private List<IUIComponent> children;

        public AbstractUIPanel (IUIComponent parent, Rectangle size, Texture2D background) : base(parent, size, background) {
            children = new List<IUIComponent>();
        }

        public List<IUIComponent> Children {
            get { return children; }
        }

        public void AddChild (IUIComponent uiComponent) {
            if (!children.Contains(uiComponent)) {
                children.Add(uiComponent);
            }
        }

        public bool RemoveChild (IUIComponent uiComponent) {
            return children.Remove(uiComponent);
        }
    }
}
