﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.UI;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DEDVisualizerRedux.Classes.UI.Input;

namespace DEDVisualizerRedux.Classes.UI {
    class Canvas : AbstractUIComponent, ICanvas {
        public delegate void LevelHandler (int level);
        private static event LevelHandler RegisterLevelEvent;
        private static event LevelHandler DeregisterLevelEvent;

        private List<IUIComponent> children;
        private Game game;
        private List<int> registeredLevels;

        public Canvas (Game game, Rectangle size, Texture2D background) : base(null, size, background) {
            children = new List<IUIComponent>();
            this.game = game;
            EventRegistry.ActiveRegistry.StateManager = new InputStateManager(this);
            registeredLevels = new List<int>();
            RegisterLevelEvent += AddLevel;
        }

        public List<IUIComponent> Children {
            get { return children; }
        }

        public List<int> RegisteredLevels {
            get { return registeredLevels; }
        }

        public Game Game {
            get { return game; }
        }

        public void AddChild (IUIComponent uiComponent) {
            if (!children.Contains(uiComponent)) {
                children.Add(uiComponent);
            }
        }

        public bool RemoveChild (IUIComponent uiComponent) {
            return children.Remove(uiComponent);
        }

        public override void Update () {
            if (game.IsActive) {
                EventRegistry.ActiveRegistry.Update();
            }
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(Background, Size, Color.White * 0.5f);
        }

        public void AddLevel(int level) {
            if (!registeredLevels.Contains(level)) {
                registeredLevels.Add(level);
                registeredLevels.Sort(comparison: (int a, int b) => { return b - a; });
            }
        }

        public void RemoveLevel(int level) {
            registeredLevels.Remove(level);
        }

        public static void ComponentAddToLevel(int level) {
            RegisterLevelEvent(level);
        }

        public static void ComponentRemovedFromLevel(int level) {
            DeregisterLevelEvent(level);
        }
    }
}
