﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.EventListeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Classes.UI.EventListeners {
    class DoubleClickListener : IEventListener {
        public delegate void DoubleClickDelegate (AbstractUIComponent uiComponent);

        public DoubleClickDelegate OnDoubleClick;

        public DoubleClickListener() {
            OnDoubleClick = OnDoubleClickImpl;
        }

        private void OnDoubleClickImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Double clicked");
        }
    }
}
