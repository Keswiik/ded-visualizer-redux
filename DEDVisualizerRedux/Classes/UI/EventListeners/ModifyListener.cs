﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.EventListeners;
using System;

namespace DEDVisualizerRedux.Classes.UI.EventListeners {
    class ModifyListener : IEventListener {
        public delegate void ModifyDelegate (AbstractUIComponent uiComponent);

        public ModifyDelegate OnModify;

        public ModifyListener() {
            OnModify = OnModifyImpl;
        }

        private void OnModifyImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Modified");
        }
    }
}
