﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.EventListeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Classes.UI.EventListeners {
    class HoverListener : IEventListener {
        public delegate void HoverDelegate (AbstractUIComponent uiComponent);

        public HoverDelegate OnHoverStart;
        public HoverDelegate OnHoverEnd;

        public HoverListener() {
            OnHoverStart = OnHoverStartImpl;
            OnHoverEnd = OnHoverEndImpl;
        }

        private void OnHoverStartImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Hover started");
        }

        private void OnHoverEndImpl (AbstractUIComponent uiComponent) {
            Console.WriteLine("Hover ended");
        }
    }
}
