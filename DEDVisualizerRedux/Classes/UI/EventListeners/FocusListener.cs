﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.EventListeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Classes.UI.EventListeners {

    class FocusListener : IEventListener {
        public delegate void FocusDelegate (AbstractUIComponent uiComponent);

        public FocusDelegate OnFocusLost;
        public FocusDelegate OnFocusGained;

        public FocusListener () {
            OnFocusLost = OnFocusLostImpl;
            OnFocusGained = OnFocusGainedImpl;
        }

        public void OnFocusGainedImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Focus gained");
        }

        public void OnFocusLostImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Focus lost");
        }
    }
}
