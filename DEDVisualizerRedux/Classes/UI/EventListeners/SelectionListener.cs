﻿using DEDVisualizerRedux.Interfaces;
using DEDVisualizerRedux.Interfaces.EventListeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DEDVisualizerRedux.Classes.UI.Abstract;

namespace DEDVisualizerRedux.Classes.UI.EventListeners {
    class SelectionListener : IEventListener {
        public delegate void SelectionDelegate (AbstractUIComponent uiComponent);

        public SelectionDelegate OnSelection;

        public SelectionListener() {
            OnSelection = OnSelectionImpl;
        }

        private void OnSelectionImpl(AbstractUIComponent uiComponent) {
            Console.WriteLine("Component selected");
        }
    }
}
