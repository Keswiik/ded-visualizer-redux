﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.EventListeners;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEDVisualizerRedux.Classes.UI.EventListeners {
    class DragListener : IEventListener {
        public delegate void DragDelegate (AbstractUIComponent uiComponent, Vector2 mouseLocation);

        public DragDelegate OnDragStart;
        public DragDelegate OnDrag;
        public DragDelegate OnDragEnd;

        public DragListener() {
            OnDragStart = OnDragStartImpl;
            OnDrag = OnDragImpl;
            OnDragEnd = OnDragEndImpl;
        }

        private void OnDragStartImpl(AbstractUIComponent uiComponent, Vector2 mouseLocation) {
            Console.WriteLine("Drag started");
        }

        private void OnDragImpl(AbstractUIComponent uIComponent, Vector2 mouseLocation) {
            Console.WriteLine("Dragging");
        }

        private void OnDragEndImpl(AbstractUIComponent uIComponent, Vector2 mouseLocation) {
            Console.WriteLine("Drag ended");
        }
    }
}
