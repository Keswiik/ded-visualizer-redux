﻿using DEDVisualizerRedux.Classes.UI.Abstract;
using DEDVisualizerRedux.Interfaces.EventListeners;
using Microsoft.Xna.Framework.Input;
using System;
using System.Diagnostics;

namespace DEDVisualizerRedux.Classes.UI.EventListeners {
    class KeyListener : IEventListener {
        public delegate void KeyDelegate (AbstractUIComponent uiComponent, Keys key);

        public KeyDelegate OnKeyPressed;
        public KeyDelegate OnKeyReleased;

        public KeyListener () {
            OnKeyPressed = OnKeyPressedImpl;
            OnKeyReleased = OnKeyReleasedImpl;
        }

        private void OnKeyPressedImpl(AbstractUIComponent uiComponent, Keys key) {
            Debug.WriteLine("Key " + key + " pressed");
        }

        private void OnKeyReleasedImpl(AbstractUIComponent uiComponent, Keys key) {
            Debug.WriteLine("Key " + key + " released");
        }
    }
}
