﻿using DEDVisualizerRedux.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DEDVisualizerRedux.Classes.UI;

namespace DEDVisualizerRedux.Classes {
    class EventData : IEventData {
        private Dictionary<string, object> eventData;
        private EventType eventType;

        public Dictionary<string, object> Data {
            get { return eventData; }
        }

        public EventType EventType {
            get { return eventType; }
            set { eventType = value; }
        }

        public EventData (EventType eventType) : this(eventType, null) { }

        public EventData(EventType eventType, Dictionary<string, object> data) {
            this.eventType = eventType;
            eventData = new Dictionary<string, object>();
            if (data != null) {
                foreach (KeyValuePair<string, object> pair in data.AsEnumerable()) {
                    eventData.Add(pair.Key, pair.Value);
                }
            }
        }

        public void AddEntry (string key, object data) {
            if (data == null) {
                throw new ArgumentNullException("Data cannot be null");
            } else if (key == null) {
                throw new ArgumentNullException("Key cannot be null");
            }

            eventData.Add(key, data);
        }

        public bool RemoveEntry (string key) {
            if (key == null) {
                throw new ArgumentNullException("Key cannot be null");
            }

            return eventData.Remove(key);
        }

        public char GetChar (string key) {
            return GetData<char>(key);
        }

        public T GetData<T> (string key) {
            if (key == null) {
                throw new ArgumentNullException("Key cannot be null");
            }

            object value = eventData[key] ?? throw new ArgumentNullException("Key does not exist");
            if (value is T) {
                return (T) value;
            } else {
                throw new ArgumentException("Key belongs to a class that is not of type " + typeof(T).Name);
            }
        }

        public double GetDouble (string key) {
            return GetData<double>(key);
        }

        public float GetFloat (string key) {
            return GetData<float>(key);
        }

        public int GetInt (string key) {
            return GetData<int>(key);
        }

        public long GetLong (string key) {
            return GetData<long>(key);
        }

        public short GetShort (string key) {
            return GetData<short>(key);
        }

        public string GetString (string key) {
            return GetData<string>(key);
        }
    }
}
